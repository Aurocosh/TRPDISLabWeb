﻿using System.Data;
using NUnit.Framework;

namespace TRPDISLabTests
{
    public static class DataTableComparator
    {
        public static void AssertThatTablesAreEqual(DataTable tbl1, DataTable tbl2)
        {
            var message = "";
            Assert.AreEqual(tbl1.Rows.Count, tbl2.Rows.Count, "Table row count mismatch");
            //Assert.AreEqual(tbl1.Columns.Count, tbl2.Columns.Count,"Table column count mismatch");
            var columnCount = tbl1.Columns.Count < tbl2.Columns.Count ? tbl1.Columns.Count : tbl2.Columns.Count;

            for (int i = 0; i < tbl1.Rows.Count; i++)
            {
                for (int c = 0; c < columnCount; c++)
                {
                    var elementOne = tbl1.Rows[i][c];
                    var elementTwo = tbl2.Rows[i][c];

                    var elementsEqual = Equals(elementOne, elementTwo);
                    if (!elementsEqual)
                    {
                        message =
                            $"Elements '{elementOne}' and '{elementTwo}' are not equal." +
                            $" Mismatch occured in row {i} and column {tbl1.Columns[c]}." +
                            $" Table one is {tbl1.TableName} and table two is {tbl2.TableName}";
                    }
                    Assert.True(elementsEqual, message);
                }
            }
        }
    }
}
