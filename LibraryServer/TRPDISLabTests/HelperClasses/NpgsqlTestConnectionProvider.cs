﻿using TRPDISLab;
using Npgsql;

namespace TRPDISLabTests
{
    public static class NpgsqlTestConnectionProvider
    {
        private const string SqlTempDbConnectionString = @"
            Host=localhost;
            Username=tester;
            Password=test;
            Database=testLibrary";
        private const string SqlTestDbConnectionString = @"
            Host=localhost;
            Username=librarian;
            Password=pass;
            Database=testLibrary";
        public static bool UseTemporaryDb { get; } = false;

        public static NpgsqlConnection GetTestNpgConnection()
        {
            return UseTemporaryDb ? new NpgsqlConnection(SqlTempDbConnectionString) : new NpgsqlConnection(SqlTestDbConnectionString);
        }

        public static void FillCountriesWithDefault(NpgsqlConnection conn)
        {
            var command = new NpgsqlCommand(Properties.Resources.CountriesSQLScript, conn);
            command.ExecuteNonQuery();
        }

        public static void FillGenresWithDefault(NpgsqlConnection conn)
        {
            var command = new NpgsqlCommand(Properties.Resources.GenresSQLScript, conn);
            command.ExecuteNonQuery();
        }

        public static void FillPublishersWithDefault(NpgsqlConnection conn)
        {
            var command = new NpgsqlCommand(Properties.Resources.PublishersSQLScript, conn);
            command.ExecuteNonQuery();
        }

    }
}
