﻿using System;
using System.Collections.Generic;
using System.Linq;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests
{
    static class TestHelper
    {
        private static readonly Dictionary<string, Type> ContainerDictionary = new Dictionary<string, Type>
        {
            {"author", typeof(Author)},
            {"book", typeof(Book)},
            {"country", typeof(Country)},
            {"genre", typeof(Genre)},
            {"atbr", typeof(AuthorToBookRelation)},
            {"publisher", typeof(Publisher)}
        };

        private static readonly Dictionary<string, string> AccessorDictionary = new Dictionary<string, string>
        {
            {"author", "AuthorsAccessor"},
            {"book", "BooksAccessor"},
            {"country", "CountriesAccessor"},
            {"genre", "GenresAccessor"},
            {"author to book", "AuthorToBookRelationAccessor"},
            {"publisher", "PublishersAccessor"}
        };

        private static readonly Dictionary<string, string> LogicDictionary = new Dictionary<string, string>
        {
            {"author", "AuthorsLogic"},
            {"author to book", "AuthorsToBooksLogic"},
            {"book", "BooksLogic"},
            {"publisher", "PublishersLogic"}
        };

        public static Type GetTypeByName(string name) => ContainerDictionary[name];
        public static string MapToAccessorClassName(string name) => AccessorDictionary[name];
        public static string MapToLogicClassName(string name) => LogicDictionary[name];

        public static string[] GetParameters(string parameterString)
        {
            var parameters = parameterString.Split(',').Select(v => v.Trim()).ToArray();
            return parameters;
        }

        public static List<List<string>> SplitDataList(string list)
        {
            var dataLists = new List<List<string>>();

            var stringList = list.Split(';').ToList();
            foreach (var dataString in stringList)
            {
                var data = dataString.Split(',').Select(v => v.Trim()).ToList();
                dataLists.Add(data);
            }

            return dataLists;
        }

        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            var a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static IEnumerable<T> CastList<T>(object input)
        {
            return (IEnumerable<T>)input;
        }
    }
}
