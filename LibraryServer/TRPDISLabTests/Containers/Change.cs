﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TRPDISLabTests.Containers
{
    public class Change
    {
        public int Row { get; }
        public int Column { get; }
        public string Value { get; }

        public static Regex ChangeRegex = new Regex(@"(\d+),(\d+):([^;]+)", RegexOptions.Compiled);

        public Change(int row, int column, string value)
        {
            Row = row;
            Column = column;
            Value = value;
        }

        public static List<Change> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<Change>();

            var changes = ChangeRegex.Matches(list);
            var changesList = (
                from Match change in changes
                let row = int.Parse(change.Groups[1].Value)
                let column = int.Parse(change.Groups[2].Value)
                let value = change.Groups[3].Value
                select new Change(row, column, value)).ToList();
            return changesList;
        }
    }
}
