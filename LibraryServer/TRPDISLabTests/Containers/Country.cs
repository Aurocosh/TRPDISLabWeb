﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests
{
    public class Country : TestObjectContainer
    {
        public int CountryId { get; set; }
        public string Name { get; set; }

        public Country(int CountryId, string Name)
        {
            this.CountryId = CountryId;
            this.Name = Name;
        }

        public static List<TestObjectContainer> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<TestObjectContainer>();

            var dataLists = TestHelper.SplitDataList(list);
            var countries = (
                from dataString in dataLists
                let id = int.Parse(dataString[0])
                let name = dataString[1]
                select new Country(id, name)).Cast<TestObjectContainer>().ToList();
            return countries;
        }

        public static DataTable ListToDataTable(List<Country> countries)
        {
            var table = new DataTable();
            table.Columns.Add("country_id", typeof(int));
            table.Columns.Add("name", typeof(string));
            foreach (var country in countries)
                table.Rows.Add(country.CountryId, country.Name);
            return table;
        }

        public override object[] GetAsObjectArray()
        {
            object[] data = { CountryId, Name };
            return data;
        }
    }
}
