﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests
{
    public class Genre : TestObjectContainer
    {
        public int GenreId { get; set; }
        public string Name { get; set; }

        public Genre(int GenreId, string Name)
        {
            this.GenreId = GenreId;
            this.Name = Name;
        }

        public static List<TestObjectContainer> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<TestObjectContainer>();

            var dataLists = TestHelper.SplitDataList(list);
            var genres = (
                from dataString in dataLists
                let id = int.Parse(dataString[0])
                let name = dataString[1]
                select new Genre(id, name)).Cast<TestObjectContainer>().ToList();
            return genres;
        }

        public static DataTable ListToDataTable(List<Genre> genres)
        {
            var table = new DataTable();
            table.Columns.Add("genre_id", typeof(int));
            table.Columns.Add("name", typeof(string));
            foreach (var genre in genres)
                table.Rows.Add(genre.GenreId, genre.Name);
            return table;
        }

        public override object[] GetAsObjectArray()
        {
            object[] data = { GenreId, Name };
            return data;
        }
    }
}
