﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPDISLabTests.Containers
{
    class Author : TestObjectContainer
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int CountryId { get; set; }

        public Author(int AuthorId, string Name, string Surname, int CountryId)
        {
            this.AuthorId = AuthorId;
            this.Name = Name;
            this.Surname = Surname;
            this.CountryId = CountryId;
        }

        public static List<TestObjectContainer> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<TestObjectContainer>();

            var dataLists = TestHelper.SplitDataList(list);
            var authors = (
                from dataString in dataLists
                let id = int.Parse(dataString[0])
                let name = dataString[1]
                let surname = dataString[2] 
                let countryId = int.Parse(dataString[3])
                select new Author(id, name, surname, countryId)).Cast<TestObjectContainer>().ToList();
            return authors;
        }

        public static DataTable ListToDataTable(List<Author> authors)
        {
            var table = new DataTable();
            table.Columns.Add("publisher_id", typeof(int));
            table.Columns.Add("name", typeof(string));
            table.Columns.Add("surname", typeof(string));
            table.Columns.Add("country_id", typeof(int));
            foreach (var author in authors)
                table.Rows.Add(author.AuthorId, author.Name, author.Surname, author.CountryId);
            return table;
        }

        public override object[] GetAsObjectArray()
        {
            object[] data = { AuthorId, Name, Surname, CountryId };
            return data;
        }
    }
}
