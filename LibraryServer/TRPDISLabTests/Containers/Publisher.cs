﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPDISLabTests.Containers
{
    public class Publisher : TestObjectContainer
    {
        public int PublisherId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public Publisher(int PublisherId, string Name, int CountryId)
        {
            this.PublisherId = PublisherId;
            this.Name = Name;
            this.CountryId = CountryId;
        }

        public static List<TestObjectContainer> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<TestObjectContainer>();

            var dataLists = TestHelper.SplitDataList(list);
            var publishers = (
                from dataString in dataLists
                let id = int.Parse(dataString[0])
                let name = dataString[1]
                let countryId = int.Parse(dataString[2])
                select new Publisher(id, name, countryId)).Cast<TestObjectContainer>().ToList();
            return publishers;
        }

        public static DataTable ListToDataTable(List<Publisher> publishers)
        {
            var table = new DataTable();
            table.Columns.Add("publisher_id", typeof(int));
            table.Columns.Add("name", typeof(string));
            table.Columns.Add("country_id", typeof(int));
            foreach (var publisher in publishers)
                table.Rows.Add(publisher.PublisherId, publisher.Name, publisher.CountryId);
            return table;
        }

        public override object[] GetAsObjectArray()
        {
            object[] data = { PublisherId, Name , CountryId};
            return data;
        }
    }
}
