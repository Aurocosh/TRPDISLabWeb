﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPDISLabTests.Containers
{
    public class AuthorToBookRelation : TestObjectContainer
    {
        public int AtbrId { get; set; }
        public int Authorid { get; set; }
        public int BookId { get; set; }

        public AuthorToBookRelation(int AtbrId, int Authorid, int BookId)
        {
            this.AtbrId = AtbrId;
            this.Authorid = Authorid;
            this.BookId = BookId;
        }

        public static List<TestObjectContainer> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<TestObjectContainer>();

            var dataLists = TestHelper.SplitDataList(list);
            var authors = (
                from dataString in dataLists
                let id = int.Parse(dataString[0])
                let authorid = int.Parse(dataString[1])
                let bookid = int.Parse(dataString[2])
                select new AuthorToBookRelation(id, authorid, bookid)).Cast<TestObjectContainer>().ToList();
            return authors;
        }

        public static DataTable ListToDataTable(List<AuthorToBookRelation> relations)
        {
            var table = new DataTable();
            table.Columns.Add("atbr_id", typeof(int));
            table.Columns.Add("author_id", typeof(int));
            table.Columns.Add("book_id", typeof(int));
            foreach (var relation in relations)
                table.Rows.Add(relation.AtbrId, relation.Authorid, relation.BookId);
            return table;
        }

        public override object[] GetAsObjectArray()
        {
            object[] data = { AtbrId, Authorid, BookId};
            return data;
        }
    }
}
