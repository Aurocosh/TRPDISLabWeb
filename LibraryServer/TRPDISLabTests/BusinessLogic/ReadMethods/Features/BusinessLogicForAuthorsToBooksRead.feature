﻿Feature: BusinessLogicForAuthorsToBooksRead
	I want to read books business logic data via relevant read method

@mytag
Scenario: BusinessLogicForAuthorsToBooksReadOne
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name  |
	| 1          | China |
	And i have inserted following "publisher" records into "example" dataset's table "publishers"
	| publisher_id | name    | country_id |
	| 1            | Pearson | 1          |
	And i have inserted following "genre" records into "example" dataset's table "genres"
	| genre_id | name            |
	| 1        | Science fiction |
	And i have inserted following "book" records into "example" dataset's table "books"
	| book_id | name                                 | genre_id | publication_date | publisher_id |
	| 1       | Do Androids Dream of Electric Sheep? | 1        | 1942-10-27       | 1            |
	And i have inserted following "author" records into "example" dataset's table "authors"
	| author_id | name | surname | country_id |
	| 1         | Jane | Reid    | 1          |
	And i have inserted following "atbr" records into "example" dataset's table "author_to_book_relation"
	| atbr_id | author_id | book_id |
	| 1       | 1         | 1       |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "publisher" accessor
	And i have updated database with data from "example" dataset via "genre" accessor
	And i have updated database with data from "example" dataset via "book" accessor
	And i have updated database with data from "example" dataset via "author" accessor
	And i have updated database with data from "example" dataset via "author to book" accessor
	And i have created new "test" dataset
	And i have used "author to book" busines logic method read on "test" dataset
	Then dataset datatables "countries, publishers, genres, books, authors, author_to_book_relation" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed





	@mytag
Scenario: BusinessLogicForAuthorsToBooksReadEmpty
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name  |
	And i have inserted following "publisher" records into "example" dataset's table "publishers"
	| publisher_id | name    | country_id |
	And i have inserted following "genre" records into "example" dataset's table "genres"
	| genre_id | name            |
	And i have inserted following "book" records into "example" dataset's table "books"
	| book_id | name                                 | genre_id | publication_date | publisher_id |
	And i have inserted following "author" records into "example" dataset's table "authors"
	| author_id | name | surname | country_id |
	And i have inserted following "atbr" records into "example" dataset's table "author_to_book_relation"
	| atbr_id | author_id | book_id |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "publisher" accessor
	And i have updated database with data from "example" dataset via "genre" accessor
	And i have updated database with data from "example" dataset via "book" accessor
	And i have updated database with data from "example" dataset via "author" accessor
	And i have updated database with data from "example" dataset via "author to book" accessor
	And i have created new "test" dataset
	And i have used "author to book" busines logic method read on "test" dataset
	Then dataset datatables "countries, publishers, genres, books, authors, author_to_book_relation" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed





@mytag
Scenario: BusinessLogicForAuthorsToBooksReadMany
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name           |
	| 1          | China          |
	| 2          | Colombia       |
	| 3          | China          |
	| 4          | Bangladesh     |
	| 5          | North Korea    |
	| 6          | Russia         |
	| 7          | Thailand       |
	| 8          | China          |
	| 9          | Brazil         |
	| 10         | China          |
	| 11         | Philippines    |
	| 12         | Afghanistan    |
	| 13         | Ukraine        |
	| 14         | Ireland        |
	| 15         | China          |
	| 16         | Ukraine        |
	| 17         | Czech Republic |
	| 18         | Cuba           |
	| 19         | China          |
	| 20         | China          |
	| 21         | Portugal       |
	And i have inserted following "publisher" records into "example" dataset's table "publishers"
	| publisher_id | name                                 | country_id |
	| 1            | Pearson                              | 4          |
	| 2            | ThomsonReuters                       | 1          |
	| 3            | RELX Group                           | 11         |
	| 4            | Wolters Kluwer                       | 4          |
	| 5            | Penguin Random House                 | 12         |
	| 6            | Phoenix Publishing and Media Company | 12         |
	| 7            | China South Publishing Group         | 3          |
	| 8            | Hachette Livre                       | 13         |
	| 9            | McGraw-Hill Education                | 10         |
	| 10           | Holtzbrinck                          | 1          |
	| 11           | Grupo Planeta                        | 18         |
	| 12           | Scholastic                           | 2          |
	| 13           | Wiley                                | 9          |
	| 14           | Cengage Learning Holdings II LP      | 8          |
	| 15           | China Publishing Group Corporation   | 20         |
	| 16           | Harper Collins                       | 5          |
	| 17           | Houghton Mifflin Harcourt            | 3          |
	| 18           | De Agostini Editore                  | 2          |
	| 19           | Oxford University Press              | 14         |
	| 20           | Springer Science and Business Media  | 6          |
	| 21           | China Education Publishing Holdings  | 7          |
	And i have inserted following "genre" records into "example" dataset's table "genres"
	| genre_id | name                   |
	| 1        | Science fiction        |
	| 2        | Satire                 |
	| 3        | Drama                  |
	| 4        | Action and Adventure   |
	| 5        | Romance                |
	| 6        | Mystery                |
	| 7        | Horror                 |
	| 8        | Self help              |
	| 9        | Health                 |
	| 10       | Guide                  |
	| 11       | Travel                 |
	| 12       | Childrens              |
	And i have inserted following "book" records into "example" dataset's table "books"
	| book_id | name                                                                   | genre_id | publication_date | publisher_id |
	| 1       | Do Androids Dream of Electric Sheep?                                   | 1        | 1942-10-27       | 5            |
	| 2       | Something Wicked This Way Comes Green Town #2                          | 12       | 1848-06-24       | 4            |
	| 3       | The Hitchhikers Guide to the Galaxy Hitchhikers Guide to the Galaxy #1 | 11       | 1978-07-11       | 20           |
	| 4       | Pride and Prejudice and Zombies Pride and Prejudice and Zombies #1     | 4        | 1847-06-08       | 17           |
	| 5       | I Was Told Thered Be Cake                                              | 10       | 1841-07-15       | 21           |
	| 6       | The Curious Incident of the Dog in the Night-Time                      | 2        | 1833-09-16       | 5            |
	| 7       | The Hollow Chocolate Bunnies of the Apocalypse                         | 7        | 1877-10-29       | 15           |
	| 8       | Eats Shoots & Leaves: The Zero Tolerance Approach to Punctuation       | 6        | 1903-03-10       | 14           |
	| 9       | To Kill a Mockingbird                                                  | 9        | 1879-01-31       | 2            |
	| 10      | The Unbearable Lightness of Being                                      | 3        | 1948-07-11       | 5            |
	| 11      | Are You There Vodka? Its Me Chelsea                                    | 3        | 1960-07-31       | 3            |
	| 12      | The Man Without Qualities                                              | 6        | 1953-05-31       | 13           |
	| 13      | Midnight in the Garden of Good and Evil: A Savannah Story              | 6        | 1918-01-28       | 9            |
	| 14      | The Perks of Being a Wallflower                                        | 4        | 1937-07-09       | 11           |
	| 15      | The Earth My Butt and Other Big Round Things                           | 5        | 1987-12-28       | 7            |
	And i have inserted following "author" records into "example" dataset's table "authors"
	| author_id | name      | surname    | country_id |
	| 1         | Jane      | Reid       | 1          |
	| 2         | Brian     | Hunt       | 10         |
	| 3         | Louise    | Clark      | 9          |
	| 4         | Marie     | Franklin   | 7          |
	| 5         | Lawrence  | Fowler     | 8          |
	| 6         | Phillip   | Bishop     | 2          |
	| 7         | Carol     | Cunningham | 17         |
	| 8         | Lillian   | Schmidt    | 1          |
	| 9         | Frank     | Russell    | 11         |
	| 10        | Anna      | Gray       | 19         |
	| 11        | Rose      | Lewis      | 21         |
	| 12        | Jose      | Reynolds   | 15         |
	| 13        | Christina | Gomez      | 3          |
	| 14        | Mildred   | Carpenter  | 20         |
	| 15        | Lori      | Simmons    | 4          |
	| 16        | Jesse     | Rogers     | 3          |
	| 17        | Daniel    | Gardner    | 5          |
	| 18        | Stephanie | Grant      | 6          |
	| 19        | Patrick   | Weaver     | 12         |
	| 20        | Robert    | Riley      | 6          |
	| 21        | Juan      | Myers      | 16         |
	And i have inserted following "atbr" records into "example" dataset's table "author_to_book_relation"
	| atbr_id | author_id | book_id |
	| 1       | 1         | 1       |
	| 2       | 1         | 2       |
	| 3       | 5         | 7       |
	| 4       | 6         | 4       |
	| 5       | 7         | 4       |
	| 6       | 2         | 9       |
	| 7       | 10        | 10      |
	| 8       | 11        | 3       |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "publisher" accessor
	And i have updated database with data from "example" dataset via "genre" accessor
	And i have updated database with data from "example" dataset via "book" accessor
	And i have updated database with data from "example" dataset via "author" accessor
	And i have updated database with data from "example" dataset via "author to book" accessor
	And i have created new "test" dataset
	And i have used "author to book" busines logic method read on "test" dataset
	Then dataset datatables "countries, publishers, genres, books, authors, author_to_book_relation" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed