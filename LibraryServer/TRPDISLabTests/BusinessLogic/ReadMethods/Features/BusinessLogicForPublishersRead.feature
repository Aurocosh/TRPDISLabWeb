﻿Feature: BusinessLogicForPublishersRead
	I want to read books business logic data via relevant read method

@mytag
Scenario: BusinessLogicForPublishersReadOne
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name  |
	| 1          | China |
	And i have inserted following "publisher" records into "example" dataset's table "publishers"
	| publisher_id | name    | country_id |
	| 1            | Pearson | 1          |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "publisher" accessor
	And i have created new "test" dataset
	And i have used "publisher" busines logic method read on "test" dataset
	Then dataset datatables "countries, publishers" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed





@mytag
Scenario: BusinessLogicForPublishersReadEmpty
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name  |
	And i have inserted following "publisher" records into "example" dataset's table "publishers"
	| publisher_id | name    | country_id |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "publisher" accessor
	And i have created new "test" dataset
	And i have used "publisher" busines logic method read on "test" dataset
	Then dataset datatables "countries, publishers" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed





@mytag
Scenario: BusinessLogicForPublishersReadMany
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name           |
	| 1          | China          |
	| 2          | Colombia       |
	| 3          | China          |
	| 4          | Bangladesh     |
	| 5          | North Korea    |
	| 6          | Russia         |
	| 7          | Thailand       |
	| 8          | China          |
	| 9          | Brazil         |
	| 10         | China          |
	| 11         | Philippines    |
	| 12         | Afghanistan    |
	| 13         | Ukraine        |
	| 14         | Ireland        |
	| 15         | China          |
	| 16         | Ukraine        |
	| 17         | Czech Republic |
	| 18         | Cuba           |
	| 19         | China          |
	| 20         | China          |
	| 21         | Portugal       |
	And i have inserted following "publisher" records into "example" dataset's table "publishers"
	| publisher_id | name                                 | country_id |
	| 1            | Pearson                              | 4          |
	| 2            | ThomsonReuters                       | 1          |
	| 3            | RELX Group                           | 11         |
	| 4            | Wolters Kluwer                       | 4          |
	| 5            | Penguin Random House                 | 12         |
	| 6            | Phoenix Publishing and Media Company | 12         |
	| 7            | China South Publishing Group         | 3          |
	| 8            | Hachette Livre                       | 13         |
	| 9            | McGraw-Hill Education                | 10         |
	| 10           | Holtzbrinck                          | 1          |
	| 11           | Grupo Planeta                        | 18         |
	| 12           | Scholastic                           | 2          |
	| 13           | Wiley                                | 9          |
	| 14           | Cengage Learning Holdings II LP      | 8          |
	| 15           | China Publishing Group Corporation   | 20         |
	| 16           | Harper Collins                       | 5          |
	| 17           | Houghton Mifflin Harcourt            | 3          |
	| 18           | De Agostini Editore                  | 2          |
	| 19           | Oxford University Press              | 14         |
	| 20           | Springer Science and Business Media  | 6          |
	| 21           | China Education Publishing Holdings  | 7          |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "publisher" accessor
	And i have created new "test" dataset
	And i have used "publisher" busines logic method read on "test" dataset
	Then dataset datatables "countries, publishers" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed