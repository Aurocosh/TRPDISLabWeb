﻿Feature: BusinessLogicForAuthorsRead
	I want to read books business logic data via relevant read method

@mytag
Scenario: BusinessLogicForAuthorsReadOne
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name    |
	| 1          | Canada  |
		
	And i have inserted following "author" records into "example" dataset's table "authors"
	| author_id | name | surname | country_id |
	| 1         | Jane | Reid    | 1          |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "author" accessor
	And i have created new "test" dataset
	And i have used "author" busines logic method read on "test" dataset
	Then dataset datatables "countries, authors" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed





	@mytag
Scenario: BusinessLogicForAuthorsReadEmpty
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name    |
	And i have inserted following "author" records into "example" dataset's table "authors"
	| author_id | name | surname | country_id |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "author" accessor
	And i have created new "test" dataset
	And i have used "author" busines logic method read on "test" dataset
	Then dataset datatables "countries, authors" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed





	@mytag
Scenario: BusinessLogicForAuthorsReadMany
	Given i have got a connection from connection factory
	And i have cleared database
	And i have created new "example" dataset

	And i have inserted following "country" records into "example" dataset's table "countries"
	| country_id | name           |
	| 1          | China          |
	| 2          | Colombia       |
	| 3          | China          |
	| 4          | Bangladesh     |
	| 5          | North Korea    |
	| 6          | Russia         |
	| 7          | Thailand       |
	| 8          | China          |
	| 9          | Brazil         |
	| 10         | China          |
	| 11         | Philippines    |
	| 12         | Afghanistan    |
	| 13         | Ukraine        |
	| 14         | Ireland        |
	| 15         | China          |
	| 16         | Ukraine        |
	| 17         | Czech Republic |
	| 18         | Cuba           |
	| 19         | China          |
	| 20         | China          |
	| 21         | Portugal       |
	And i have inserted following "author" records into "example" dataset's table "authors"
	| author_id | name      | surname    | country_id |
	| 1         | Jane      | Reid       | 1          |
	| 2         | Brian     | Hunt       | 10         |
	| 3         | Louise    | Clark      | 9          |
	| 4         | Marie     | Franklin   | 7          |
	| 5         | Lawrence  | Fowler     | 8          |
	| 6         | Phillip   | Bishop     | 2          |
	| 7         | Carol     | Cunningham | 17         |
	| 8         | Lillian   | Schmidt    | 1          |
	| 9         | Frank     | Russell    | 11         |
	| 10        | Anna      | Gray       | 19         |
	| 11        | Rose      | Lewis      | 21         |
	| 12        | Jose      | Reynolds   | 15         |
	| 13        | Christina | Gomez      | 3          |
	| 14        | Mildred   | Carpenter  | 20         |
	| 15        | Lori      | Simmons    | 4          |
	| 16        | Jesse     | Rogers     | 3          |
	| 17        | Daniel    | Gardner    | 5          |
	| 18        | Stephanie | Grant      | 6          |
	| 19        | Patrick   | Weaver     | 12         |
	| 20        | Robert    | Riley      | 6          |
	| 21        | Juan      | Myers      | 16         |

	And i have updated database with data from "example" dataset via "country" accessor
	And i have updated database with data from "example" dataset via "author" accessor
	And i have created new "test" dataset
	And i have used "author" busines logic method read on "test" dataset
	Then dataset datatables "countries, authors" in "test" dataset should be equal to releveant datatables in dataset "example"
	And connection should be closed