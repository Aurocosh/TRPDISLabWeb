﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.1.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace TRPDISLabTests.BusinessLogic.UpdateMethods.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("BusinessLogicForAuthorsUpdate")]
    public partial class BusinessLogicForAuthorsUpdateFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "BusinessLogicForAuthorsUpdate.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "BusinessLogicForAuthorsUpdate", "\tI want to read books business logic data via relevant read method", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("BusinessLogicForAuthorsUpdateOne")]
        [NUnit.Framework.CategoryAttribute("mytag")]
        public virtual void BusinessLogicForAuthorsUpdateOne()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("BusinessLogicForAuthorsUpdateOne", new string[] {
                        "mytag"});
#line 5
this.ScenarioSetup(scenarioInfo);
#line 6
 testRunner.Given("i have got a connection from connection factory", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 7
 testRunner.And("i have cleared database", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 8
 testRunner.And("i have created new \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "country_id",
                        "name"});
            table1.AddRow(new string[] {
                        "1",
                        "Canada"});
#line 10
 testRunner.And("i have inserted following \"country\" records into \"example\" dataset\'s table \"count" +
                    "ries\"", ((string)(null)), table1, "And ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "author_id",
                        "name",
                        "surname",
                        "country_id"});
            table2.AddRow(new string[] {
                        "1",
                        "Jane",
                        "Reid",
                        "1"});
#line 13
 testRunner.And("i have inserted following \"author\" records into \"example\" dataset\'s table \"author" +
                    "s\"", ((string)(null)), table2, "And ");
#line 17
 testRunner.And("i have used \"author\" busines logic method update on \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "country_id",
                        "name",
                        "state"});
            table3.AddRow(new string[] {
                        "1",
                        "China",
                        "updated"});
#line 18
 testRunner.And("i have changed records in \"example\" dataset\'s table \"countries\" as follow", ((string)(null)), table3, "And ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "author_id",
                        "name",
                        "surname",
                        "country_id",
                        "state"});
            table4.AddRow(new string[] {
                        "1",
                        "Pete",
                        "Jenkins",
                        "1",
                        "updated"});
#line 21
 testRunner.And("i have changed records in \"example\" dataset\'s table \"authors\" as follow", ((string)(null)), table4, "And ");
#line 25
 testRunner.And("i have used \"author\" busines logic method update on \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
 testRunner.When("i have created new \"test\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 27
 testRunner.And("i have loaded data into \"test\" dataset via \"country\" accessor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 28
 testRunner.And("i have loaded data into \"test\" dataset via \"author\" accessor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 29
 testRunner.Then("dataset datatables \"countries, authors\" in \"test\" dataset should be equal to rele" +
                    "veant datatables in dataset \"example\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 30
 testRunner.And("connection should be closed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("BusinessLogicForAuthorsUpdateEmpty")]
        [NUnit.Framework.CategoryAttribute("mytag")]
        public virtual void BusinessLogicForAuthorsUpdateEmpty()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("BusinessLogicForAuthorsUpdateEmpty", new string[] {
                        "mytag"});
#line 37
this.ScenarioSetup(scenarioInfo);
#line 38
 testRunner.Given("i have got a connection from connection factory", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 39
 testRunner.And("i have cleared database", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 40
 testRunner.And("i have created new \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "country_id",
                        "name"});
            table5.AddRow(new string[] {
                        "1",
                        "Canada"});
#line 42
 testRunner.And("i have inserted following \"country\" records into \"example\" dataset\'s table \"count" +
                    "ries\"", ((string)(null)), table5, "And ");
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "author_id",
                        "name",
                        "surname",
                        "country_id"});
            table6.AddRow(new string[] {
                        "1",
                        "Jane",
                        "Reid",
                        "1"});
#line 45
 testRunner.And("i have inserted following \"author\" records into \"example\" dataset\'s table \"author" +
                    "s\"", ((string)(null)), table6, "And ");
#line 49
 testRunner.And("i have used \"author\" busines logic method update on \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "country_id",
                        "name",
                        "state"});
#line 50
 testRunner.And("i have changed records in \"example\" dataset\'s table \"countries\" as follow", ((string)(null)), table7, "And ");
#line hidden
            TechTalk.SpecFlow.Table table8 = new TechTalk.SpecFlow.Table(new string[] {
                        "author_id",
                        "name",
                        "surname",
                        "country_id",
                        "state"});
#line 52
 testRunner.And("i have changed records in \"example\" dataset\'s table \"authors\" as follow", ((string)(null)), table8, "And ");
#line 55
 testRunner.And("i have used \"author\" busines logic method update on \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 56
 testRunner.When("i have created new \"test\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 57
 testRunner.And("i have loaded data into \"test\" dataset via \"country\" accessor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 58
 testRunner.And("i have loaded data into \"test\" dataset via \"author\" accessor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 59
 testRunner.Then("dataset datatables \"countries, authors\" in \"test\" dataset should be equal to rele" +
                    "veant datatables in dataset \"example\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 60
 testRunner.And("connection should be closed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("BusinessLogicForAuthorsUpdateMany")]
        [NUnit.Framework.CategoryAttribute("mytag")]
        public virtual void BusinessLogicForAuthorsUpdateMany()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("BusinessLogicForAuthorsUpdateMany", new string[] {
                        "mytag"});
#line 67
this.ScenarioSetup(scenarioInfo);
#line 68
 testRunner.Given("i have got a connection from connection factory", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 69
 testRunner.And("i have cleared database", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 70
 testRunner.And("i have created new \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table9 = new TechTalk.SpecFlow.Table(new string[] {
                        "country_id",
                        "name"});
            table9.AddRow(new string[] {
                        "1",
                        "China"});
            table9.AddRow(new string[] {
                        "2",
                        "Colombia"});
            table9.AddRow(new string[] {
                        "3",
                        "China"});
            table9.AddRow(new string[] {
                        "4",
                        "Bangladesh"});
            table9.AddRow(new string[] {
                        "5",
                        "North Korea"});
            table9.AddRow(new string[] {
                        "6",
                        "Russia"});
            table9.AddRow(new string[] {
                        "7",
                        "Thailand"});
            table9.AddRow(new string[] {
                        "8",
                        "China"});
            table9.AddRow(new string[] {
                        "9",
                        "Brazil"});
            table9.AddRow(new string[] {
                        "10",
                        "China"});
            table9.AddRow(new string[] {
                        "11",
                        "Philippines"});
            table9.AddRow(new string[] {
                        "12",
                        "Afghanistan"});
            table9.AddRow(new string[] {
                        "13",
                        "Ukraine"});
            table9.AddRow(new string[] {
                        "14",
                        "Ireland"});
            table9.AddRow(new string[] {
                        "15",
                        "China"});
            table9.AddRow(new string[] {
                        "16",
                        "Ukraine"});
            table9.AddRow(new string[] {
                        "17",
                        "Czech Republic"});
            table9.AddRow(new string[] {
                        "18",
                        "Cuba"});
            table9.AddRow(new string[] {
                        "19",
                        "China"});
            table9.AddRow(new string[] {
                        "20",
                        "China"});
            table9.AddRow(new string[] {
                        "21",
                        "Portugal"});
#line 72
 testRunner.And("i have inserted following \"country\" records into \"example\" dataset\'s table \"count" +
                    "ries\"", ((string)(null)), table9, "And ");
#line hidden
            TechTalk.SpecFlow.Table table10 = new TechTalk.SpecFlow.Table(new string[] {
                        "author_id",
                        "name",
                        "surname",
                        "country_id"});
            table10.AddRow(new string[] {
                        "1",
                        "Jane",
                        "Reid",
                        "1"});
            table10.AddRow(new string[] {
                        "2",
                        "Brian",
                        "Hunt",
                        "10"});
            table10.AddRow(new string[] {
                        "3",
                        "Louise",
                        "Clark",
                        "9"});
            table10.AddRow(new string[] {
                        "4",
                        "Marie",
                        "Franklin",
                        "7"});
            table10.AddRow(new string[] {
                        "5",
                        "Lawrence",
                        "Fowler",
                        "8"});
            table10.AddRow(new string[] {
                        "6",
                        "Phillip",
                        "Bishop",
                        "2"});
            table10.AddRow(new string[] {
                        "7",
                        "Carol",
                        "Cunningham",
                        "19"});
            table10.AddRow(new string[] {
                        "8",
                        "Lillian",
                        "Schmidt",
                        "1"});
            table10.AddRow(new string[] {
                        "9",
                        "Frank",
                        "Russell",
                        "11"});
            table10.AddRow(new string[] {
                        "10",
                        "Anna",
                        "Gray",
                        "13"});
            table10.AddRow(new string[] {
                        "11",
                        "Rose",
                        "Lewis",
                        "21"});
            table10.AddRow(new string[] {
                        "12",
                        "Jose",
                        "Reynolds",
                        "15"});
            table10.AddRow(new string[] {
                        "13",
                        "Christina",
                        "Gomez",
                        "3"});
            table10.AddRow(new string[] {
                        "14",
                        "Mildred",
                        "Carpenter",
                        "15"});
            table10.AddRow(new string[] {
                        "15",
                        "Lori",
                        "Simmons",
                        "4"});
            table10.AddRow(new string[] {
                        "16",
                        "Jesse",
                        "Rogers",
                        "3"});
            table10.AddRow(new string[] {
                        "17",
                        "Daniel",
                        "Gardner",
                        "5"});
            table10.AddRow(new string[] {
                        "18",
                        "Stephanie",
                        "Grant",
                        "6"});
            table10.AddRow(new string[] {
                        "19",
                        "Patrick",
                        "Weaver",
                        "12"});
            table10.AddRow(new string[] {
                        "20",
                        "Robert",
                        "Riley",
                        "6"});
            table10.AddRow(new string[] {
                        "21",
                        "Juan",
                        "Myers",
                        "16"});
#line 95
 testRunner.And("i have inserted following \"author\" records into \"example\" dataset\'s table \"author" +
                    "s\"", ((string)(null)), table10, "And ");
#line 119
 testRunner.And("i have used \"author\" busines logic method update on \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            TechTalk.SpecFlow.Table table11 = new TechTalk.SpecFlow.Table(new string[] {
                        "country_id",
                        "name",
                        "state"});
            table11.AddRow(new string[] {
                        "1",
                        "Country 1",
                        "updated"});
            table11.AddRow(new string[] {
                        "2",
                        "Country 2",
                        "updated"});
            table11.AddRow(new string[] {
                        "4",
                        "Bangladesh",
                        "deleted"});
            table11.AddRow(new string[] {
                        "6",
                        "Country 3",
                        "updated"});
            table11.AddRow(new string[] {
                        "14",
                        "Country 4",
                        "updated"});
            table11.AddRow(new string[] {
                        "16",
                        "Portugal",
                        "updated"});
            table11.AddRow(new string[] {
                        "19",
                        "Portugal",
                        "updated"});
            table11.AddRow(new string[] {
                        "20",
                        "China",
                        "deleted"});
            table11.AddRow(new string[] {
                        "21",
                        "Portugal",
                        "deleted"});
#line 120
 testRunner.And("i have changed records in \"example\" dataset\'s table \"countries\" as follow", ((string)(null)), table11, "And ");
#line hidden
            TechTalk.SpecFlow.Table table12 = new TechTalk.SpecFlow.Table(new string[] {
                        "author_id",
                        "name",
                        "surname",
                        "country_id",
                        "status"});
            table12.AddRow(new string[] {
                        "16",
                        "Jesse",
                        "Rogers",
                        "3",
                        "deleted"});
            table12.AddRow(new string[] {
                        "17",
                        "A",
                        "A",
                        "1",
                        "updated"});
            table12.AddRow(new string[] {
                        "18",
                        "B",
                        "B",
                        "1",
                        "updated"});
            table12.AddRow(new string[] {
                        "19",
                        "Patrick",
                        "Weaver",
                        "12",
                        "deleted"});
            table12.AddRow(new string[] {
                        "20",
                        "C",
                        "C",
                        "1",
                        "updated"});
#line 131
 testRunner.And("i have changed records in \"example\" dataset\'s table \"authors\" as follow", ((string)(null)), table12, "And ");
#line 139
 testRunner.And("i have used \"author\" busines logic method update on \"example\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 140
 testRunner.When("i have created new \"test\" dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 141
 testRunner.And("i have loaded data into \"test\" dataset via \"country\" accessor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 142
 testRunner.And("i have loaded data into \"test\" dataset via \"author\" accessor", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 143
 testRunner.Then("dataset datatables \"countries, authors\" in \"test\" dataset should be equal to rele" +
                    "veant datatables in dataset \"example\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 144
 testRunner.And("connection should be closed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
