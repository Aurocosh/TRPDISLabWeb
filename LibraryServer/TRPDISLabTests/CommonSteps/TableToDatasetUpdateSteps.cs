﻿using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;

namespace TRPDISLabTests.CommonSteps
{
    [Binding]
    public class TableToDatasetUpdateSteps
    {
        [Given(@"i have changed records in ""(.*)"" dataset's table ""(.*)"" as follow")]
        public void WhenIHaveChangedRecordsInDatasetSTableAsFollow(string dataSetName, string dataTableName, Table table)
        {
            var dataset = ScenarioContext.Current.Get<LibraryDataSet>(dataSetName);
            var dataTable = dataset.Tables[dataTableName];

            var header = table.Header.Select(v => v).ToList();
            var indexColumn = header.First();
            var actionColumn = header.Last();
            header.Remove(indexColumn);
            header.Remove(actionColumn);

            foreach (var row in table.Rows)
            {
                var index = row[indexColumn];
                var action = row[actionColumn];


                var nullTableRow = dataTable.Select(indexColumn + "=" + index).FirstOrDefault();


                var tableRow = dataTable.Select(indexColumn + "=" + index).First();

                if (action == "deleted")
                {
                    tableRow.Delete();
                }
                else if (action == "updated")
                {
                    foreach (var column in header)
                    {
                        tableRow[column] = row[column];
                    }
                }
                else
                {
                    Assert.True(false,"Invalid action: " + action);
                }
            }
        }
    }
}
