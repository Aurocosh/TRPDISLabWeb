﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using Spring.Context.Support;



namespace TRPDISLabTests.CommonSteps
{
    [Binding]
    public class AccessorUsageSteps
    {
        [Given(@"i have updated database with data from ""(.*)"" dataset via ""(.*)"" accessor")]
        public void GivenIHaveUpdatedDatabaseWithDataFromDatasetViaCountriesAccessor(string dataSetName, string accessorType)
        {
            var ctx = ContextRegistry.GetContext();
            var typeName = TestHelper.MapToAccessorClassName(accessorType);
            var accessor = (AbstractAccessor)ctx.GetObject(typeName);

            var dataSet = (LibraryDataSet)ScenarioContext.Current[dataSetName];
            var connection = (AbstractConnection)ScenarioContext.Current["connection"];
            Assert.NotNull(accessor,"Invalid acessor provided");

            var transaction = connection.BeginTransaction();
            accessor.Update(connection, transaction, dataSet);
            transaction.Commit();
        }

        [When(@"i have loaded data into ""(.*)"" dataset via ""(.*)"" accessor")]
        public void WhenIHaveLoadedDataIntoDatasetViaAccessor(string dataSetName, string accessorType)
        {
            var ctx = ContextRegistry.GetContext();
            var typeName = TestHelper.MapToAccessorClassName(accessorType);
            var accessor = (AbstractAccessor)ctx.GetObject(typeName);

            var dataSet = (LibraryDataSet)ScenarioContext.Current[dataSetName];
            var connection = (AbstractConnection)ScenarioContext.Current["connection"];
            Assert.NotNull(accessor, "Invalid acessor provided");

            var transaction = connection.BeginTransaction();
            accessor.Read(connection, transaction, dataSet);
            transaction.Rollback();
        }

    }
}
