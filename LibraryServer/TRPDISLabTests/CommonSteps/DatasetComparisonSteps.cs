﻿using System;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;

namespace TRPDISLabTests.CommonSteps
{
    [Binding]
    public class BusinessLogicForBooksReadSteps
    {
        [Then(@"dataset datatables ""(.*)"" in ""(.*)"" dataset should be equal to releveant datatables in dataset ""(.*)""")]
        public void ThenDatasetDatatablesInDatasetShouldBeEqualToReleveantDatatablesInDataset(string tableList, string firstDataSetName, string secondDataSetName)
        {
            var tables = tableList.Split(',').Select(v=>TestHelper.UppercaseFirst(v.Trim())).ToList();
            var firstDataSet = (LibraryDataSet)ScenarioContext.Current[firstDataSetName];
            var secondDataSet = (LibraryDataSet)ScenarioContext.Current[secondDataSetName];

            foreach (var table in tables)
            {
                var firstTable = firstDataSet.Tables[table];
                var secondTable = secondDataSet.Tables[table];

                DataTableComparator.AssertThatTablesAreEqual(firstTable, secondTable);
            }
        }
    }
}
