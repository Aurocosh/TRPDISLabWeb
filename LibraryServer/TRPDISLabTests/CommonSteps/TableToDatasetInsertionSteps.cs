﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TRPDISLab;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.CommonSteps
{
    [Binding]
    public class TableToDatasetInsertionSteps
    {
        [Given(@"i have inserted following ""(.*)"" records into ""(.*)"" dataset's table ""(.*)""")]
        public void GivenIHaveInsertedFollowingRecordsIntoDatasetSTable(string typeName, string dataSetName, string dataTableName, Table table)
        {
            var dataSet = (LibraryDataSet)ScenarioContext.Current[dataSetName];
            dataTableName = TestHelper.UppercaseFirst(dataTableName);
            var dataTable = dataSet.Tables[dataTableName];

            var type = TestHelper.GetTypeByName(typeName);
            var typeOfContext = typeof(TableHelperExtensionMethods);
            var method = typeOfContext.GetMethod("CreateSet", new[] { typeof(Table) });
            var genericMethod = method.MakeGenericMethod(type);
            var objectSet = genericMethod.Invoke(table, new object[]{table});
            var objectList = (IEnumerable)objectSet;

            foreach (var itemObject in objectList)
            {
                var item = (TestObjectContainer)itemObject;
                var itemAsArray = item.GetAsObjectArray();
                dataTable.Rows.Add(itemAsArray);
            }
        }
    }




}
