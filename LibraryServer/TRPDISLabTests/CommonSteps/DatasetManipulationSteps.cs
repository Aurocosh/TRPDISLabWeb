﻿using System;
using TechTalk.SpecFlow;
using TRPDISLab;

namespace TRPDISLabTests.CommonSteps
{
    [Binding]
    public class DatasetManipulationSteps
    {
        [When(@"i have created new ""(.*)"" dataset")]
        [Given(@"i have created new ""(.*)"" dataset")]
        public void GivenIHaveCreatedNewDataset(string datasetName)
        {
            ScenarioContext.Current[datasetName] = new LibraryDataSet();
        }
    }
}
