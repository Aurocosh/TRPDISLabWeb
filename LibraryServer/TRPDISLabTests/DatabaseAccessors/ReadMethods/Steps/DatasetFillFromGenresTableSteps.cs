﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods
{
    [Binding]
    public class DatasetFillFromGenresTableSteps
    {
        [When(@"i have created datatable ""(.*)"" from genres")]
        public void WhenIHaveCreatedDatatableFromGenres(string dataTableName)
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var genres = containerList.Cast<Genre>().ToList();
            ScenarioContext.Current[dataTableName] = Genre.ListToDataTable(genres);
        }

        [Given(@"i filled genres dataset from database")]
        [When(@"i filled genres dataset from database")]
        public void WhenILoadedAuthorsToDatasetFromDatabase()
        {
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataset = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var genresAccessor = new GenresAccessor();
            var transaction = сonnection.BeginTransaction();
            genresAccessor.Read(сonnection, transaction, dataset);
            transaction.Commit();
        }
    }
}
