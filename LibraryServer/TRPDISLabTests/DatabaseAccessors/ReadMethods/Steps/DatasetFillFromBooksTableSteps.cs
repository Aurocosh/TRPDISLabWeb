﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods
{
    [Binding]
    public class DatasetFillFromBooksTableSteps
    {
        [When(@"i have created datatable ""(.*)"" from books")]
        public void WhenIHaveCreatedDatatableFromBooks(string dataTableName)
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var books = containerList.Cast<Book>().ToList();
            ScenarioContext.Current[dataTableName] = Book.ListToDataTable(books);
        }

        [Given(@"i filled books dataset from database")]
        [When(@"i filled books dataset from database")]
        public void WhenILoadedAuthorsToDatasetFromDatabase()
        {
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataset = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var booksAccessor = new BooksAccessor();
            var transaction = сonnection.BeginTransaction();
            booksAccessor.Read(сonnection, transaction, dataset);
            transaction.Commit();
        }
    }
}
