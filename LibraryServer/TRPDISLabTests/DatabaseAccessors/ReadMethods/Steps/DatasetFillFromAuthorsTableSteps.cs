﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Steps
{
    [Binding]
    public class DatasetFillFromAuthorsTableSteps
    {
        [When(@"i have created datatable ""(.*)"" from authors")]
        public void WhenIHaveCreatedDatatableFromAuthors(string tableName)
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var authors = containerList.Cast<Author>().ToList();
            ScenarioContext.Current[tableName] = Author.ListToDataTable(authors);
        }

        [Given(@"i filled authors dataset from database")]
        [When(@"i filled authors dataset from database")]
        public void WhenILoadedAuthorsToDatasetFromDatabase()
        {
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataset = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var authorsAccessor = new AuthorsAccessor();
            var transaction = сonnection.BeginTransaction();
            authorsAccessor.Read(сonnection, transaction, dataset);
            transaction.Commit();
        }
    }
}
