﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Steps
{
    [Binding]
    public class DatasetFillFromCountriesTableSteps
    {
        [When(@"i have created datatable ""(.*)"" from countries")]
        public void WhenIHaveCreatedDatatableFromCountries(string dataTableName)
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var countries = containerList.Cast<Country>().ToList();
            ScenarioContext.Current[dataTableName] = Country.ListToDataTable(countries);
        }

        [Given(@"i filled countries dataset from database")]
        [When(@"i filled countries dataset from database")]
        public void WhenILoadedAuthorsToDatasetFromDatabase()
        {
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataset = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var countriesAccessor = new CountriesAccessor();
            var transaction = сonnection.BeginTransaction();
            countriesAccessor.Read(сonnection, transaction, dataset);
            transaction.Commit();
        }
    }
}
