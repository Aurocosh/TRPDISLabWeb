﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using TechTalk.SpecFlow;
using TRPDISLab;
namespace TRPDISLabTests.DatabaseAccessors.ReadMethods
{
    [Binding]
    public class DatasetPreparation
    {
        [Given(@"I provided valid database connection")]
        public void GivenIProvidedValidDatabaseConnection()
        {
            var connection = new AbstractConnection(NpgsqlTestConnectionProvider.GetTestNpgConnection());
            connection.Open();

            ScenarioContext.Current.Add("connection", connection);
        }

        [Given(@"i have got a connection from connection factory")]
        public void GivenIHaveGotAConnectionFromConnectionFactory()
        {
            var connection = ConnectionFactory.GetConnection();
            connection.Open();

            ScenarioContext.Current.Add("connection", connection);
        }

        [Given(@"i prepared test database")]
        public void GivenIPreparedTestDatabase()
        {
            if (NpgsqlTestConnectionProvider.UseTemporaryDb)
                GivenICreatedTempSchema();
            else
                GivenIHaveClearedTables();
        }

        [Given(@"i created temp schema")]
        public void GivenICreatedTempSchema()
        {
            var connection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dbScript = Properties.Resources.TestDbCreationScript;
            var command = new NpgsqlCommand(dbScript, connection.Connection);
            command.ExecuteNonQuery();
        }

        [Given(@"i have cleared database")]
        public void GivenIHaveClearedTables()
        {
            var tablesToClear = new List<string>
            {
                "author_to_book_relation",
                "authors",
                "books",
                "publishers",
                "genres",
                "countries"
            };

            var connection = (AbstractConnection)ScenarioContext.Current["connection"];
            var transaction = connection.BeginTransaction();
            foreach (var table in tablesToClear)
            {
                var deleteCommandText = "DELETE FROM " + table;
                var command = new NpgsqlCommand(deleteCommandText, connection.Connection);
                command.ExecuteNonQuery();
            }
            transaction.Commit();
        }


        [Given(@"i have closed connection")]
        [Then(@"connection should be closed")]
        public void ThenConnectionShouldBeClosed()
        {
            var connection = (AbstractConnection)ScenarioContext.Current["connection"];
            connection.Close();
        }
    }
}
