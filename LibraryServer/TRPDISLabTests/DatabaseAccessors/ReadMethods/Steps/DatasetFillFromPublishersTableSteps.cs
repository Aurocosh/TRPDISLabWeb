﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Steps
{
    [Binding]
    public class DatasetFillFromPublishersTableSteps
    {
        [When(@"i have created datatable ""(.*)"" from publishers")]
        public void WhenIHaveCreatedDatatableFromPublishers(string dataTableName)
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var publishers = containerList.Cast<Publisher>().ToList();
            ScenarioContext.Current[dataTableName] = Publisher.ListToDataTable(publishers);
        }

        [Given(@"i filled publishers dataset from database")]
        [When(@"i filled publishers dataset from database")]
        public void WhenILoadedAuthorsToDatasetFromDatabase()
        {
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataset = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var publishersAccessor = new PublishersAccessor();
            var transaction = сonnection.BeginTransaction();
            publishersAccessor.Read(сonnection, transaction, dataset);
            transaction.Commit();
        }
    }
}
