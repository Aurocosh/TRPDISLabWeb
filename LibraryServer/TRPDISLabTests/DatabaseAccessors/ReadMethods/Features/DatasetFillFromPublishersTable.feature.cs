﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.1.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("DatasetFillFromPublishersTable")]
    public partial class DatasetFillFromPublishersTableFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "DatasetFillFromPublishersTable.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "DatasetFillFromPublishersTable", "\tI want to load data from pg database table publishers with publishersAccessor", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("PublishersDataSetFill")]
        [NUnit.Framework.CategoryAttribute("mytag")]
        [NUnit.Framework.TestCaseAttribute("", new string[0])]
        [NUnit.Framework.TestCaseAttribute("1, \'Pearson\', 56", new string[0])]
        [NUnit.Framework.TestCaseAttribute("2, \'ThomsonReuters\', 133; 3, \'RELX Group\', 70", new string[0])]
        [NUnit.Framework.TestCaseAttribute("4, \'Wolters Kluwer\', 69; 5, \'Penguin Random House\', 38; 6, \'Phoenix Publishing an" +
            "d Media Company\', 148", new string[0])]
        [NUnit.Framework.TestCaseAttribute("7, \'China South Publishing Group\', 118; 8, \'Hachette Livre\', 109; 9, \'McGraw-Hill" +
            " Education\', 92; 10, \'Holtzbrinck\', 83; 11, \'Grupo Planeta\', 29; 12, \'Scholastic" +
            "\', 138", new string[0])]
        [NUnit.Framework.TestCaseAttribute(@"13, 'Wiley', 81; 14, 'Cengage Learning Holdings II LP', 124; 15, 'China Publishing Group Corporation', 52; 16, 'Harper Collins', 114; 17, 'Houghton Mifflin Harcourt', 116; 18, 'De Agostini Editore', 80; 19, 'Oxford University Press', 14; 20, 'Springer Science and Business Media', 64; 21, 'China Education Publishing Holdings', 35", new string[0])]
        public virtual void PublishersDataSetFill(string dataString, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "mytag"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("PublishersDataSetFill", @__tags);
#line 5
this.ScenarioSetup(scenarioInfo);
#line 6
 testRunner.Given("I provided valid database connection", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 7
 testRunner.And("i prepared test database", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 8
 testRunner.And("i created new dataset", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 9
 testRunner.And(string.Format("i loaded publishers from \"{0}\"", dataString), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 10
 testRunner.And("i have determined publishers dependecy on countries table", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 11
 testRunner.And("I have inserted dependecies in database table \"countries\", which have 2 columns \"" +
                    "country_id,name\" with \"country_id\" as key value. Default values \"\'country\'\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 12
 testRunner.And("i have prepared insert query for \"publishers\" table with columns \"publisher_id,na" +
                    "me,country_id\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 13
 testRunner.And("i have added parameters \"publisher_id,name,country_id\" with types \"Integer,Varcha" +
                    "r,Integer\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 14
 testRunner.And("i have prepared data for insertion", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 15
 testRunner.And("i have executed insert query", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 16
 testRunner.When("i filled publishers dataset from database", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 17
 testRunner.And("i have created datatable \"dataTableExample\" from publishers", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 18
 testRunner.And("i loaded dataset table \"publishers\" into \"dataTableResult\" datatable", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 19
 testRunner.Then("example datatable should be equal to result datatable", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 20
 testRunner.And("connection should be closed", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
