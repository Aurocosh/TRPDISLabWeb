﻿Feature: DatasetFillFromCountriesTable
	I want to load data from pg database table countries with countriesAccessor

@mytag
Scenario Outline: CountriesDataSetFill
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded countries from "<dataString>"
	And i have prepared insert query for "countries" table with columns "country_id,name"
	And i have added parameters "country_id,name" with types "Integer,Varchar"
	And i have prepared data for insertion
	And i have executed insert query
	When i filled countries dataset from database
	And i have created datatable "dataTableExample" from countries
	And i loaded dataset table "countries" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

	Examples: 
| dataString                                                                                                         |
|                                                                                                                    |
| 1, China                                                                                                           |
| 2, Colombia; 3, China                                                                                              |
| 4, Bangladesh; 5, North Korea; 6, Russia                                                                           |
| 7, Thailand; 8, China; 9, Brazil; 10, China; 11, Philippines; 12, Afghanistan                                      |
| 13, Ukraine; 14, Ireland; 15, China; 16, Ukraine; 17, Czech Republic; 18, Cuba; 19, China; 20, China; 21, Portugal |