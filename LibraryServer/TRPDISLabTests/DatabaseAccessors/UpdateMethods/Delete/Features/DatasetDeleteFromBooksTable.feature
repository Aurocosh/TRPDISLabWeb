﻿Feature: DatasetDeleteFromBooksTable
	I want to load data from pg database table books with booksAccessor delete some rows and update original database

@mytag
Scenario Outline: BooksDataSetDelete
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded books from "<dataString>"
	And i have determined books dependecy on genres table
	And I have inserted dependecies in database table "genres", which have 2 columns "genre_id,name" with "genre_id" as key value. Default values "'country'"
	And i have determined books dependecy on publishers table
	And i have inserted in "countries" table row with columns "country_id,name" and values "1,'country'"
	And I have inserted dependecies in database table "publishers", which have 3 columns "publisher_id,name,country_id" with "publisher_id" as key value. Default values "'publisher',1"
	And i have prepared insert query for "books" table with columns "book_id,name,genre_id,publication_date,publisher_id"
	And i have added parameters "book_id,name,genre_id,publication_date,publisher_id" with types "Integer,Varchar,Integer,Date,Integer"
	And i have prepared data for insertion
	And i have executed insert query
	And i filled books dataset from database
	And i have deleted several "<rows>" from dataset table "books"
	When i have called update method of booksAccessor
	And i loaded dataset table "books" into "dataTableExample" datatable
	And i loaded database table "books" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

Examples: 
| dataString                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | rows  |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |       |
| 1, Do Androids Dream of Electric Sheep?, 20, 1942-10-27, 5                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 0     |
| 2, Something Wicked This Way Comes Green Town #2, 31, 1848-06-24, 4; 3, The Hitchhikers Guide to the Galaxy Hitchhikers Guide to the Galaxy #1, 19, 1978-07-11, 34                                                                                                                                                                                                                                                                                                                                                                                                                                   | 0     |
| 4, Pride and Prejudice and Zombies Pride and Prejudice and Zombies #1, 4, 1847-06-08, 33; 5, I Was Told Thered Be Cake, 10, 1841-07-15, 38; 6, The Curious Incident of the Dog in the Night-Time, 19, 1833-09-16, 5                                                                                                                                                                                                                                                                                                                                                                                  | 1     |
| 7, The Hollow Chocolate Bunnies of the Apocalypse, 30, 1877-10-29, 15; 8, Eats Shoots & Leaves: The Zero Tolerance Approach to Punctuation, 20, 1903-03-10, 31; 9, To Kill a Mockingbird, 9, 1879-01-31, 57; 10, The Unbearable Lightness of Being, 3, 1948-07-11, 5; 11, Are You There Vodka? Its Me Chelsea, 29, 1960-07-31, 3; 12, The Man Without Qualities, 6, 1953-05-31, 37; 13, Midnight in the Garden of Good and Evil: A Savannah Story, 14, 1918-01-28, 47; 14, The Perks of Being a Wallflower, 28, 1937-07-09, 28; 15, The Earth My Butt and Other Big Round Things, 21, 1987-12-28, 52 | 3,4,5 |