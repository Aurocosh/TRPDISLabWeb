﻿Feature: DatasetUpdateGenresTable
	I want to load data from pg database table genres with genresAccessor change some rows and update original database

@mytag
Scenario Outline: GenresDataSetUpdate
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded genres from "<dataString>"
	And i have prepared insert query for "genres" table with columns "genre_id,name"
	And i have added parameters "genre_id,name" with types "Integer,Varchar"
	And i have prepared data for insertion
	And i have executed insert query
	And i filled genres dataset from database
	And i have applied several "<changes>" to dataset table "genres"
	When i have called update method of genresAccessor
	And i loaded dataset table "genres" into "dataTableExample" datatable
	And i loaded database table "genres" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

Examples: 
| dataString                                                                                                                                                                                       | changes                               |
|                                                                                                                                                                                                  |                                       |
| 1, 'Science fiction'                                                                                                                                                                             | 0,1:SAadFKASaasdasdasd                |
| 2, 'Satire';3, 'Drama'                                                                                                                                                                           | 0,1:China;1,1:Some country            |
| 4, 'Action and Adventure';5, 'Romance';6, 'Mystery'                                                                                                                                              | 0,1:China;1,1:Some country;2,1:asdasd |
| 7, 'Horror';8, 'Self help';9, 'Health';10, 'Guide';11, 'Travel';12, 'Children''s';13, 'Religion'                                                                                                 | 3,1:asdasdasd;4,1:asdasdasd;1,1:12312 |
| 14, 'Spirituality & New Age';15, 'Science';16, 'History';17, 'Math';18, 'Anthology';19, 'Poetry'                                                                                                 | 1,1:21wasdcas;2,2;asdasdasdasd        |
| 20, 'Encyclopedias';21, 'Dictionaries';22, 'Comics';23, 'Art';24, 'Cookbooks';25, 'Diaries';26, 'Journals';27, 'Prayer books';28, 'Series';29, 'Trilogy';30, 'Biographies';31, 'Autobiographies' | 4,1:adcccs;5,2;dfgdfgdf               |
