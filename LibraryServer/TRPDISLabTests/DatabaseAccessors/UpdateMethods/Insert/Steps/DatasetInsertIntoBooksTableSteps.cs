﻿using System;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLabTests.DatabaseAccessors.UpdateMethods.Insert.Steps
{
    [Binding]
    public class DatasetInsertIntoBooksTableSteps
    {
        [When(@"i have called update method of booksAccessor")]
        public void WhenIHaveCalledUpdateMethodOfBooksAccessor()
        {
            var booksAccessor = new BooksAccessor();
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataSet = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var transaction = сonnection.BeginTransaction();
            booksAccessor.Update(сonnection, transaction, dataSet);
            transaction.Commit();
        }
    }
}
