﻿using System;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLabTests.DatabaseAccessors.UpdateMethods.Insert.Steps
{
    [Binding]
    public class DatasetInsertIntoPublishersTableSteps
    {
        [When(@"i have called update method of publishersAccessor")]
        public void WhenIHaveCalledUpdateMethodOfPublishersAccessor()
        {
            var countriesAccessor = new PublishersAccessor();
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataSet = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var transaction = сonnection.BeginTransaction();
            countriesAccessor.Update(сonnection, transaction, dataSet);
            transaction.Commit();
        }
    }
}
