﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.UpdateMethods.Insert.Steps
{
    [Binding]
    public class DatasetInsertIntoAuthorsTableSteps
    {
        [When(@"i have called update method of authorsAccessor")]
        public void WhenIHaveCalledUpdateMethodOfAuthorsAccessor()
        {
            var authorsAccessor = new AuthorsAccessor();
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataSet = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var transaction = сonnection.BeginTransaction();
            authorsAccessor.Update(сonnection, transaction, dataSet);
            transaction.Commit();
        }
    }
}
