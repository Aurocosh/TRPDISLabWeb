﻿using System;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLabTests.DatabaseAccessors.UpdateMethods.Insert.Steps
{
    [Binding]
    public class DatasetInsertIntoGenresTableSteps
    {
        [When(@"i have called update method of genresAccessor")]
        public void WhenIHaveCalledUpdateMethodOfGenresAccessor()
        {
            var countriesAccessor = new GenresAccessor();
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataSet = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var transaction = сonnection.BeginTransaction();
            countriesAccessor.Update(сonnection, transaction, dataSet);
            transaction.Commit();
        }
    }
}
