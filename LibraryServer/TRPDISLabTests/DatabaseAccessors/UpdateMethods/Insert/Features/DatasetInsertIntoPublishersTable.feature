﻿Feature: DatasetInsertIntoPublishersTable
	I want to insert data into pg database table publishers with dataset and publishersAccessor

@mytag
Scenario Outline: PublishersDataSetInsert
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded publishers from "<dataString>"
	And i have determined publishers dependecy on countries table
	And I have inserted dependecies in database table "countries", which have 2 columns "country_id,name" with "country_id" as key value. Default values "'country'"
	And i have prepared data for insertion
	And i have inserted data into dataset table "publishers"
	When i have called update method of publishersAccessor
	And i loaded dataset table "publishers" into "dataTableExample" datatable
	And i loaded database table "publishers" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

Examples: 
| dataString                                                                                                                                                                                                                                                                                                                                 |
|                                                                                                                                                                                                                                                                                                                                            |
| 1, 'Pearson', 56                                                                                                                                                                                                                                                                                                                           |
| 2, 'ThomsonReuters', 133; 3, 'RELX Group', 70                                                                                                                                                                                                                                                                                              |
| 4, 'Wolters Kluwer', 69; 5, 'Penguin Random House', 38; 6, 'Phoenix Publishing and Media Company', 148                                                                                                                                                                                                                                     |
| 7, 'China South Publishing Group', 118; 8, 'Hachette Livre', 109; 9, 'McGraw-Hill Education', 92; 10, 'Holtzbrinck', 83; 11, 'Grupo Planeta', 29; 12, 'Scholastic', 138                                                                                                                                                                    |
| 13, 'Wiley', 81; 14, 'Cengage Learning Holdings II LP', 124; 15, 'China Publishing Group Corporation', 52; 16, 'Harper Collins', 114; 17, 'Houghton Mifflin Harcourt', 116; 18, 'De Agostini Editore', 80; 19, 'Oxford University Press', 14; 20, 'Springer Science and Business Media', 64; 21, 'China Education Publishing Holdings', 35 |