﻿Feature: DatasetInsertIntoGenresTable
	I want to insert data into pg database table genres with dataset and genresAccessor

@mytag
Scenario Outline: CountriesDataSetInsert
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded genres from "<dataString>"
	And i have prepared data for insertion
	And i have inserted data into dataset table "genres"
	When i have called update method of genresAccessor
	And i loaded dataset table "genres" into "dataTableExample" datatable
	And i loaded database table "genres" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

	Examples: 
| dataString                                                                                                                                                                                       |
|                                                                                                                                                                                                  |
| 1, 'Science fiction'                                                                                                                                                                             |
| 2, 'Satire';3, 'Drama'                                                                                                                                                                           |
| 4, 'Action and Adventure';5, 'Romance';6, 'Mystery'                                                                                                                                              |
| 7, 'Horror';8, 'Self help';9, 'Health';10, 'Guide';11, 'Travel';12, 'Children''s';13, 'Religion'                                                                                                 |
| 14, 'Spirituality & New Age';15, 'Science';16, 'History';17, 'Math';18, 'Anthology';19, 'Poetry'                                                                                                 |
| 20, 'Encyclopedias';21, 'Dictionaries';22, 'Comics';23, 'Art';24, 'Cookbooks';25, 'Diaries';26, 'Journals';27, 'Prayer books';28, 'Series';29, 'Trilogy';30, 'Biographies';31, 'Autobiographies' |
