﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.CommonSteps.Steps
{
    [Binding]
    public class TableDependencyPreparationSteps
    {
        [Given(@"i have determined authors dependecy on countries table")]
        public void GivenIHaveDeterminedAuthorsDependecyOnCountriesTable()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var authors = containerList.Cast<Author>().ToList();
            var ids = new HashSet<int>(authors.Select(b => b.CountryId)).ToList();
            ScenarioContext.Current["dependencyIds"] = ids;
        }

        [Given(@"i have determined author to book relations dependecy on book table")]
        public void GivenIHaveDeterminedAuthorToBookRelationsDependecyOnBookTable()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var authorToBookRelations = containerList.Cast<AuthorToBookRelation>().ToList();
            var ids = new HashSet<int>(authorToBookRelations.Select(b => b.BookId)).ToList();
            ScenarioContext.Current["dependencyIds"] = ids;
        }

        [Given(@"i have determined author to book relations dependecy on authors table")]
        public void GivenIHaveDeterminedAuthorToBookRelationsDependecyOnAuthorsTable()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var authorToBookRelations = containerList.Cast<AuthorToBookRelation>().ToList();
            var ids = new HashSet<int>(authorToBookRelations.Select(b => b.Authorid)).ToList();
            ScenarioContext.Current["dependencyIds"] = ids;
        }

        [Given(@"i have determined books dependecy on genres table")]
        public void GivenIHaveDeterminedBooksDependecyOnGenresTable()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var books = containerList.Cast<Book>().ToList();

            var ids = new HashSet<int>(books.Select(b => b.GenreId)).ToList();
            ScenarioContext.Current["dependencyIds"] = ids;
        }

        [Given(@"i have determined books dependecy on publishers table")]
        public void GivenIHaveDeterminedBooksDependecyOnPublishersTable()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var books = containerList.Cast<Book>().ToList();

            var ids = new HashSet<int>(books.Select(b => b.PublisherId)).ToList();
            ScenarioContext.Current["dependencyIds"] = ids;
        }

        [Given(@"i have determined publishers dependecy on countries table")]
        public void GivenIHaveDeterminedPublishersDependecyOnCountriesTable()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var publishers = containerList.Cast<Publisher>().ToList();

            var ids = new HashSet<int>(publishers.Select(b => b.CountryId)).ToList();
            ScenarioContext.Current["dependencyIds"] = ids;
        }







    }
}
