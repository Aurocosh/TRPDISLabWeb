﻿using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.CommonSteps.Steps
{
    [Binding]
    public class DataLoadingSteps
    {
        [Given(@"i created new dataset")]
        public void GivenICreatedNewDataset()
        {
            ScenarioContext.Current["dataset"] = new LibraryDataSet();
        }

        [Given(@"i loaded authors from ""(.*)""")]
        public void GivenILoadedAuthorsFrom(string dataString)
        {
            ScenarioContext.Current["containerList"] = Author.DeserializeList(dataString);
        }

        [Given(@"i loaded author to book relations from ""(.*)""")]
        public void GivenILoadedAuthorToBookRelationsFrom(string dataString)
        {
            ScenarioContext.Current["containerList"] = AuthorToBookRelation.DeserializeList(dataString);
        }

        [Given(@"i loaded books from ""(.*)""")]
        public void GivenILoadedBooksFrom(string booksSerialized)
        {
            ScenarioContext.Current["containerList"] = Book.DeserializeList(booksSerialized);
        }

        [Given(@"i loaded countries from ""(.*)""")]
        public void GivenILoadedCountriesFrom(string countriesSerialized)
        {
            ScenarioContext.Current["containerList"] = Country.DeserializeList(countriesSerialized);
        }

        [Given(@"i loaded genres from ""(.*)""")]
        public void GivenILoadedGenresFrom(string genresSerialized)
        {
            ScenarioContext.Current["containerList"] = Genre.DeserializeList(genresSerialized);
        }

        [Given(@"i loaded publishers from ""(.*)""")]
        public void GivenILoadedPublishersFrom(string publishersSerialized)
        {
            ScenarioContext.Current["containerList"] = Publisher.DeserializeList(publishersSerialized);
        }
    }
}
