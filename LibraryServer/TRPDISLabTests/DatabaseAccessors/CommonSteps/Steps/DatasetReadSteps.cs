﻿using System;
using System.Data;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;

namespace TRPDISLabTests.DatabaseAccessors.CommonSteps.Steps
{
    [Binding]
    public class DatasetReadSteps
    {
        [Given(@"i loaded dataset table ""(.*)"" into ""(.*)"" datatable")]
        [When(@"i loaded dataset table ""(.*)"" into ""(.*)"" datatable")]
        public void GivenILoadedDatasetTableIntoDatatable(string tableName, string dataTableName)
        {
            var dataSet = (LibraryDataSet)ScenarioContext.Current["dataset"];
            tableName = TestHelper.UppercaseFirst(tableName);
            ScenarioContext.Current[dataTableName] = dataSet.Tables[tableName];
        }

        [Then(@"example datatable should be equal to result datatable")]
        public void ThenExampleDatatableShouldBeEqualToResultDatatable()
        {
            var dataTableExample = (DataTable)ScenarioContext.Current["dataTableExample"];
            var dataTableResult = (DataTable)ScenarioContext.Current["dataTableResult"];

            DataTableComparator.AssertThatTablesAreEqual(dataTableExample, dataTableResult);
        }
    }
}
