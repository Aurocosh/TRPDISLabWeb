﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;
using NpgsqlTypes;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Steps
{
    [Binding]
    public class DatabaseInsertSteps
    {
        private static readonly Dictionary<string, int> ParameterDictionary = new Dictionary<string, int>
        {
            {"Integer", (int)NpgsqlDbType.Integer},
            {"integer", (int)NpgsqlDbType.Integer},
            {"Varchar", (int)NpgsqlDbType.Varchar},
            {"varchar", (int)NpgsqlDbType.Varchar},
            {"Date", (int)NpgsqlDbType.Date},
            {"date", (int)NpgsqlDbType.Date},
        };

        [Given(@"i have prepared insert query for ""(.*)"" table with columns ""(.*)""")]
        public void GivenIHavePreparedInsertQueryForTableWithColumns(string table, string columns)
        {
            var valuesArray = TestHelper.GetParameters(columns);
            valuesArray = valuesArray.Select(v => "@" + v).ToArray();
            var values = string.Join(",", valuesArray);

            var insertCommand = $@"
                INSERT INTO {table}({columns})
                VALUES({values})";

            var conn = (AbstractConnection)ScenarioContext.Current["connection"];
            var command = new NpgsqlCommand(insertCommand, conn.Connection);
            ScenarioContext.Current["command"] = command;
        }
        
        [Given(@"i have added parameters ""(.*)"" with types ""(.*)""")]
        public void GivenIHaveAddedParametersWithTypes(string parametersString, string typesString)
        {
            var command = (NpgsqlCommand) ScenarioContext.Current["command"];

            var parameters = TestHelper.GetParameters(parametersString);
            parameters = parameters.Select(v => "@" + v).ToArray();
            var types = typesString.Split(',').Select(v => ParameterDictionary[v]).ToArray();

            Assert.AreEqual(parameters.Length,types.Length,"Parameter count not equal to type count");
            ScenarioContext.Current["parameters"] = parameters;

            var limit = parameters.Length;
            for (int i = 0; i < limit; i++)
                command.Parameters.AddWithValue(parameters[i], types[i]);
        }

        [Given(@"i have prepared data for insertion")]
        public void GivenIHavePreparedDataForInsertion()
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var parameterData = containerList.Select(v => v.GetAsObjectArray()).ToArray();
            ScenarioContext.Current["parameterData"] = parameterData;
        }

        [Given(@"i have executed insert query")]
        public void GivenIHaveExecutedInsertQueryWithParameters()
        {
            var parameterData = (object[][]) ScenarioContext.Current["parameterData"];
            var parameters = (string[]) ScenarioContext.Current["parameters"];
            var command = (NpgsqlCommand) ScenarioContext.Current["command"];
            var conn = (AbstractConnection) ScenarioContext.Current["connection"];

            var rowCount = parameterData.Length;
            if (rowCount == 0) return;
            var columnCount = parameterData.First().Length;

            Assert.AreEqual(columnCount, parameters.Length, "Parameter data columnCount count not equal parameter count");

            var transaction = conn.BeginTransaction();
            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    command.Parameters[parameters[j]].Value = parameterData[i][j];
                }
                command.ExecuteNonQuery();
            }
            transaction.Commit();
        }
    }
}
