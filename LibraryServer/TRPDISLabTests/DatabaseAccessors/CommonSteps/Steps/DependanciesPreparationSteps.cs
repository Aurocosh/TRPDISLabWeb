﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;
using TechTalk.SpecFlow;
using TRPDISLab;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Steps
{
    [Binding]
    public class DependanciesPreparationSteps
    {
        [Given(@"i have inserted in ""(.*)"" table row with columns ""(.*)"" and values ""(.*)""")]
        public void GivenIHaveInsertedInTableRowWithColumnsAndValues(string tableName, string columns, string values)
        {
            var lset = (LibraryDataSet)ScenarioContext.Current["dataset"];
            var сonn = (AbstractConnection)ScenarioContext.Current["connection"];

            var insertCommand = $@"
            INSERT INTO {tableName}({columns})
            VALUES({values})";
            var command = new NpgsqlCommand(insertCommand, сonn.Connection);

            var transaction = сonn.BeginTransaction();
            command.ExecuteNonQuery();
            transaction.Commit();

            var relatedTable = lset.Tables[TestHelper.UppercaseFirst(tableName)];
            var paramsList = values.Split(',').Select(v => (object)v).ToArray();
            relatedTable.Rows.Add(paramsList);
        }

        [Given(@"I have inserted dependecies in database table ""(.*)"", which have (.*) columns ""(.*)"" with ""(.*)"" as key value\. Default values ""(.*)""")]
        public void GivenIHaveInsertedDependeciesInDatabaseTableWhichHaveColumnsWithAsKeyValue_DefaultValues(string tableName, int columnCount, string columns, string key, string extraValues)
        {
            var lset = (LibraryDataSet)ScenarioContext.Current["dataset"];
            var сonn = (AbstractConnection)ScenarioContext.Current["connection"];
            var ids = (List<int>)ScenarioContext.Current["dependencyIds"];

            var values = "@" + key + "," + extraValues;

            var insertCommand = $@"
            INSERT INTO {tableName}({columns})
            VALUES({values})";
            var command = new NpgsqlCommand(insertCommand, сonn.Connection);
            command.Parameters.Add(new NpgsqlParameter("@" + key, NpgsqlDbType.Integer));

            var transaction = сonn.BeginTransaction();
            foreach (var id in ids)
            {
                command.Parameters[key].Value = id;
                command.ExecuteNonQuery();
            }
            transaction.Commit();
            
            var relatedTable = lset.Tables[TestHelper.UppercaseFirst(tableName)];
            foreach (var id in ids)
            {
                var paramsList = new List<object> {id, Enumerable.Range(1, columnCount-1).Select(v=>"''")};
                relatedTable.Rows.Add(paramsList.ToArray());
            }
            relatedTable.Rows.Add();
        }
    }
}
