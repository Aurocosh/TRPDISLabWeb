﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.CommonSteps.Steps
{
    [Binding]
    public class DatasetInsertCommonSteps
    {
        private static readonly Dictionary<string, Type> TypeDictionary = new Dictionary<string, Type>
        {
            {"Integer", typeof(int)},
            {"integer", typeof(int)},
            {"Varchar", typeof(string)},
            {"varchar", typeof(string)},
            {"Date", typeof(DateTime)},
            {"date", typeof(DateTime)},
        };

        [Given(@"i have inserted data into dataset table ""(.*)""")]
        public void GivenIHaveInsertedDataIntoDatasetTable(string tablename)
        {
            var lset = (LibraryDataSet)ScenarioContext.Current["dataset"];
            tablename = TestHelper.UppercaseFirst(tablename);
            var table = lset.Tables[tablename];

            var parameterData = (object[][])ScenarioContext.Current["parameterData"];
            var rowsCount = parameterData.Length;

            for (int i = 0; i < rowsCount; i++)
                table.Rows.Add(parameterData[i]);
        }

        [Given(@"i have applied several ""(.*)"" to dataset table ""(.*)""")]
        public void GivenIHaveAppliedSeveralToDatasetTable(string changesListString, string tablename)
        {
            var lset = (LibraryDataSet)ScenarioContext.Current["dataset"];
            tablename = TestHelper.UppercaseFirst(tablename);
            var table = lset.Tables[tablename];

            var changesList = Change.DeserializeList(changesListString);

            foreach (var change in changesList)
                table.Rows[change.Row][change.Column] = change.Value;
        }

        [Given(@"i have deleted several ""(.*)"" from dataset table ""(.*)""")]
        public void GivenIHaveDeletedSeveralFromDatasetTable(string rowsString, string tablename)
        {
            var lset = (LibraryDataSet)ScenarioContext.Current["dataset"];
            tablename = TestHelper.UppercaseFirst(tablename);
            var table = lset.Tables[tablename];

            var rowsCount = table.Rows.Count;
            if(rowsCount == 0) return;
            var rowsToDelete = rowsString.Split(',').Select(int.Parse).Distinct().Where(x => x < rowsCount-1).OrderByDescending(x => x).ToArray();
            rowsCount = rowsToDelete.Length;

            for (int i = 0; i < rowsCount; i++)
                table.Rows[i].Delete();
        }


        //[Given(@"I have inserted dependecies in dataset table ""(.*)"", which have (.*) columns\. Default values ""(.*)""")]
        //public void GivenIHaveInsertedDependeciesInDatasetTableWhichHaveColumns_DefaultValues(string tablename, int columnCount, string p2)
        //{
        //    var lset = (LibraryDataSet)ScenarioContext.Current["dataset"];
        //    tablename = TestHelper.UppercaseFirst(tablename);
        //    var table = lset.Tables[tablename];

        //    var parameterData = (object[][])ScenarioContext.Current["parameterData"];
        //    var rowsCount = parameterData.Length;

        //    for (int i = 0; i < rowsCount; i++)
        //    {
        //        table.Rows.Add(parameterData[i]);

        //    }
        //}
    }
}
