﻿using System;
using System.Collections.Generic;
using System.Linq;
using Npgsql;

namespace TRPDISLab.IdProvider
{
    public class IdRangeProvider : ILibraryIdProvider
    {
        private readonly int _idBlockSize;
        private static object _authorsLock;
        private static object _atbrsLock;
        private static object _booksLock;
        private static object _publishersLock;

        private const string SelectQuery = "SELECT value FROM global_settings WHERE name=@name";
        private const string UpdateQuery = "UPDATE global_settings SET value=value+@step WHERE name=@name";

        public IdRangeProvider()
        {
            _idBlockSize = 10;

            _authorsLock = new object();
            _atbrsLock = new object();
            _booksLock = new object();
            _publishersLock = new object();
        }

        public List<int> GetAuthorsIds() => GetIds("last_author_id", _authorsLock);
        public List<int> GetAtbrsIds() => GetIds("last_atbr_id", _atbrsLock);
        public List<int> GetBooksIds() => GetIds("last_book_id", _booksLock);
        public List<int> GetPublishersIds() => GetIds("last_publisher_id", _publishersLock);

        private List<int> GetIds(string type, object locker)
        {
            lock (locker)
            {
                using (var connection = ConnectionFactory.GetConnection())
                {
                    connection.Open();
                    var transaction = connection.BeginTransaction();

                    var selectCommand = new NpgsqlCommand(SelectQuery, connection.Connection, transaction.Transaction);
                    selectCommand.Parameters.AddWithValue("@name", type);

                    var updateCommand = new NpgsqlCommand(UpdateQuery, connection.Connection, transaction.Transaction);
                    updateCommand.Parameters.AddWithValue("@step", _idBlockSize);
                    updateCommand.Parameters.AddWithValue("@name", type);
                    try
                    {
                        List<int> ids;
                        using (var reader = selectCommand.ExecuteReader())
                        {
                            if (!reader.Read()) return new List<int>();

                            var lastId = (int)reader["value"];
                            ids = Enumerable.Range(lastId, _idBlockSize).ToList();
                        }
                        updateCommand.ExecuteNonQuery();
                        transaction.Commit();
                        return ids;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}