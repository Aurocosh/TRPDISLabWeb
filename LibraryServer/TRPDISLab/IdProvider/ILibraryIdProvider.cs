﻿using System.Collections.Generic;

namespace TRPDISLab.IdProvider
{
    public interface ILibraryIdProvider
    {
        List<int> GetAuthorsIds();
        List<int> GetAtbrsIds();
        List<int> GetBooksIds();
        List<int> GetPublishersIds();
    }
}