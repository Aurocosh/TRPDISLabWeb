﻿using Npgsql;
using System.Configuration;

namespace TRPDISLab
{
    public class ConnectionFactory
    {
        public static AbstractConnection GetConnection()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["PostgresConnection"].ConnectionString;
            var npgconnection = new NpgsqlConnection(connectionString);
            return new AbstractConnection(npgconnection);
        }
    }
}
