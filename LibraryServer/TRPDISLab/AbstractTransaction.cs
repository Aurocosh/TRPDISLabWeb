﻿using System;
using Npgsql;

namespace TRPDISLab
{
    public class AbstractTransaction : IDisposable
    {
        public NpgsqlTransaction Transaction { get; set; }

        public AbstractTransaction(NpgsqlTransaction transaction)
        {
            Transaction = transaction;
        }

        public void Commit()
        {
            Transaction.Commit();
        }

        public void Rollback()
        {
            Transaction.Rollback();
        }

        public void Dispose()
        {
            Transaction.Rollback();
        }
    }
}
