﻿namespace TRPDISLab.Service
{
    public interface ILibrary
    {
        LibraryDataSet ReadAuthors();
        void UpdateAuthors(LibraryDataSet dataSet);
        LibraryDataSet ReadAuthorsXBooks();
        void UpdateAuthorsXBooks(LibraryDataSet dataSet);
        LibraryDataSet ReadBooks();
        void UpdateBooks(LibraryDataSet dataSet);
        LibraryDataSet ReadPublishers();
        void UpdatePublishers(LibraryDataSet dataSet);
    }
}