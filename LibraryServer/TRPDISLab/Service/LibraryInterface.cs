﻿using TRPDISLab.BusinessLogic;

namespace TRPDISLab.Service
{
    public class LibraryInterface: ILibrary
    {
        public AbstractLogic AutorsLogic { get; }
        public AbstractLogic AutorsToBooksLogic { get; }
        public AbstractLogic BooksLogic { get; }
        public AbstractLogic PublishersLogic { get; }

        public LibraryInterface(AbstractLogic autorsLogic, AbstractLogic autorsToBooksLogic, AbstractLogic booksLogic, AbstractLogic publishersLogic)
        {
            AutorsLogic = autorsLogic;
            AutorsToBooksLogic = autorsToBooksLogic;
            BooksLogic = booksLogic;
            PublishersLogic = publishersLogic;
        }

        public LibraryDataSet ReadAuthors()
        {
            var dataSet = new LibraryDataSet();
            AutorsLogic.Read(dataSet);
            return dataSet;
        }

        public void UpdateAuthors(LibraryDataSet dataSet)
        {
            AutorsLogic.Update(dataSet);
        }

        public LibraryDataSet ReadAuthorsXBooks()
        {
            var dataSet = new LibraryDataSet();
            AutorsToBooksLogic.Read(dataSet);
            return dataSet;
        }

        public void UpdateAuthorsXBooks(LibraryDataSet dataSet)
        {
            AutorsToBooksLogic.Update(dataSet);
        }

        public LibraryDataSet ReadBooks()
        {
            var dataSet = new LibraryDataSet();
            BooksLogic.Read(dataSet);
            return dataSet;
        }

        public void UpdateBooks(LibraryDataSet dataSet)
        {
            BooksLogic.Update(dataSet);
        }

        public LibraryDataSet ReadPublishers()
        {
            var dataSet = new LibraryDataSet();
            PublishersLogic.Read(dataSet);
            return dataSet;
        }

        public void UpdatePublishers(LibraryDataSet dataSet)
        {
            PublishersLogic.Update(dataSet);
        }
    }
}