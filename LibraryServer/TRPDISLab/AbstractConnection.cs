﻿using System;
using Npgsql;

namespace TRPDISLab
{
    public class AbstractConnection : IDisposable
    {
        public NpgsqlConnection Connection { get; set; }

        public AbstractConnection(NpgsqlConnection connection)
        {
            Connection = connection;
        }

        public void Open()
        {
            Connection.Open();
        }

        public void Close()
        {
            Connection.Close();
        }

        public AbstractTransaction BeginTransaction()
        {
            return new AbstractTransaction(Connection.BeginTransaction());
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
