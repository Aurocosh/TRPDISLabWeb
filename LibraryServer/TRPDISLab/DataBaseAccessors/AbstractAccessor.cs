﻿namespace TRPDISLab.DataBaseAccessors
{
    public abstract class AbstractAccessor
    {
        public abstract void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s);
        public abstract void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s);
    }
}
