﻿using Npgsql;
using NpgsqlTypes;

namespace TRPDISLab.DataBaseAccessors
{
    public class AuthorsAccessor : AbstractAccessor
    {
        public override void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string selectCommand = "SELECT * FROM authors ORDER BY author_id";
            adapter.SelectCommand = new NpgsqlCommand(selectCommand, c.Connection, t.Transaction);
            adapter.Fill(s, "Authors");
        }

        public override void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string updateCommand = @"
                UPDATE authors 
                SET name=@name, surname=@surname, country_id=@country_id 
                WHERE author_id=@author_id";
            adapter.UpdateCommand = new NpgsqlCommand(updateCommand, c.Connection, t.Transaction);
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@surname", NpgsqlDbType.Varchar, 128, "surname"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@author_id", NpgsqlDbType.Integer, sizeof(int), "author_id"));

            const string deleteCommand = "DELETE FROM authors WHERE author_id=@author_id";
            adapter.DeleteCommand = new NpgsqlCommand(deleteCommand, c.Connection, t.Transaction);
            adapter.DeleteCommand.Parameters.Add(new NpgsqlParameter("@author_id", NpgsqlDbType.Integer, sizeof(int), "author_id"));

            const string insertCommand = @"
                INSERT INTO authors(author_id, name, surname, country_id)
                VALUES(@author_id, @name, @surname, @country_id)";
            adapter.InsertCommand = new NpgsqlCommand(insertCommand, c.Connection, t.Transaction);
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@author_id", NpgsqlDbType.Integer, sizeof(int), "author_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, -1, "name"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@surname", NpgsqlDbType.Varchar, -1, "surname"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));

            int rowsUpdate = adapter.Update(s, "Authors");
        }
    }
}
