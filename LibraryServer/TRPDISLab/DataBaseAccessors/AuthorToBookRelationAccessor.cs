﻿using Npgsql;
using NpgsqlTypes;

namespace TRPDISLab.DataBaseAccessors
{
    public class AuthorToBookRelationAccessor : AbstractAccessor
    {
        public override void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string selectCommand = "SELECT * FROM author_to_book_relation ORDER BY atbr_id";
            adapter.SelectCommand = new NpgsqlCommand(selectCommand, c.Connection, t.Transaction);
            adapter.Fill(s, "author_to_book_relation");
        }

        public override void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string updateCommand = @"
                UPDATE author_to_book_relation
                SET author_id=@author_id,book_id=@book_id
                WHERE atbr_id=@atbr_id";
            adapter.UpdateCommand = new NpgsqlCommand(updateCommand, c.Connection, t.Transaction);
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@author_id", NpgsqlDbType.Integer, sizeof(int), "author_id"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@book_id", NpgsqlDbType.Integer, sizeof(int), "book_id"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@atbr_id", NpgsqlDbType.Integer, sizeof(int), "atbr_id"));

            const string deleteCommand = "DELETE FROM author_to_book_relation WHERE atbr_id=@atbr_id";
            adapter.DeleteCommand = new NpgsqlCommand(deleteCommand, c.Connection, t.Transaction);
            adapter.DeleteCommand.Parameters.Add(new NpgsqlParameter("@atbr_id", NpgsqlDbType.Integer, sizeof(int), "atbr_id"));

            const string insertCommand = @"
                INSERT INTO author_to_book_relation(atbr_id,author_id,book_id)
                VALUES(@atbr_id,@author_id,@book_id)";
            adapter.InsertCommand = new NpgsqlCommand(insertCommand, c.Connection, t.Transaction);
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@atbr_id", NpgsqlDbType.Integer, sizeof(int), "atbr_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@author_id", NpgsqlDbType.Integer, sizeof(int), "author_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@book_id", NpgsqlDbType.Integer, sizeof(int), "book_id"));

            adapter.Update(s, "author_to_book_relation");
        }
    }
}
