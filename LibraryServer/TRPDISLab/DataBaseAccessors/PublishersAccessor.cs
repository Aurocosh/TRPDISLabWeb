﻿using Npgsql;
using NpgsqlTypes;

namespace TRPDISLab.DataBaseAccessors
{
    public class PublishersAccessor : AbstractAccessor
    {
        public override void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string selectCommand = "SELECT * FROM publishers ORDER BY publisher_id";
            adapter.SelectCommand = new NpgsqlCommand(selectCommand, c.Connection, t.Transaction);
            adapter.Fill(s, "publishers");
        }

        public override void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string updateCommand = @"
                UPDATE publishers
                SET name=@name,country_id=@country_id
                WHERE publisher_id=@publisher_id";
            adapter.UpdateCommand = new NpgsqlCommand(updateCommand, c.Connection, t.Transaction);
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@publisher_id", NpgsqlDbType.Integer, sizeof(int), "publisher_id"));

            const string deleteCommand = "DELETE FROM publishers WHERE publisher_id=@publisher_id";
            adapter.DeleteCommand = new NpgsqlCommand(deleteCommand, c.Connection, t.Transaction);
            adapter.DeleteCommand.Parameters.Add(new NpgsqlParameter("@publisher_id", NpgsqlDbType.Integer, sizeof(int), "publisher_id"));

            const string insertCommand = @"
                INSERT INTO publishers(publisher_id,name,country_id)
                VALUES(@publisher_id,@name,@country_id)";
            adapter.InsertCommand = new NpgsqlCommand(insertCommand, c.Connection, t.Transaction);
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@publisher_id", NpgsqlDbType.Integer, sizeof(int), "publisher_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));

            adapter.Update(s, "publishers");
        }
    }
}
