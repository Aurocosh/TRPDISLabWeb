﻿using Npgsql;
using NpgsqlTypes;

namespace TRPDISLab.DataBaseAccessors
{
    public class CountriesAccessor : AbstractAccessor
    {
        public override void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string selectCommand = "SELECT * FROM countries ORDER BY country_id";
            adapter.SelectCommand = new NpgsqlCommand(selectCommand, c.Connection, t.Transaction);
            adapter.Fill(s, "countries");
        }

        public override void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string updateCommand = @"
                UPDATE countries
                SET name=@name
                WHERE country_id=@country_id";
            adapter.UpdateCommand = new NpgsqlCommand(updateCommand, c.Connection, t.Transaction);
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));

            const string deleteCommand = "DELETE FROM countries WHERE country_id=@country_id";
            adapter.DeleteCommand = new NpgsqlCommand(deleteCommand, c.Connection, t.Transaction);
            adapter.DeleteCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));

            const string insertCommand = @"
                INSERT INTO countries(country_id, name)
                VALUES(@country_id, @name)";
            adapter.InsertCommand = new NpgsqlCommand(insertCommand, c.Connection, t.Transaction);
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@country_id", NpgsqlDbType.Integer, sizeof(int), "country_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));

            adapter.Update(s, "Countries");
            int tst = 1;

        }
    }
}
