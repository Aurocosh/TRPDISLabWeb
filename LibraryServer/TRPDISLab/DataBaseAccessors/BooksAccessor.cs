﻿using Npgsql;
using NpgsqlTypes;

namespace TRPDISLab.DataBaseAccessors
{
    public class BooksAccessor : AbstractAccessor
    {
        public override void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string selectCommand = "SELECT * FROM books ORDER BY book_id";
            adapter.SelectCommand = new NpgsqlCommand(selectCommand, c.Connection, t.Transaction);
            adapter.Fill(s, "books");
        }

        public override void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string updateCom = @"
                UPDATE books 
                SET name=@name,genre_id=@genre_id,publication_date=@publication_date,publisher_id=@publisher_id 
                WHERE book_id=@book_id";
            adapter.UpdateCommand = new NpgsqlCommand(updateCom, c.Connection, t.Transaction);
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@genre_id", NpgsqlDbType.Integer, sizeof(int), "genre_id"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@publication_date", NpgsqlDbType.Date, 128, "publication_date"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@publisher_id", NpgsqlDbType.Integer, sizeof(int), "publisher_id"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@book_id", NpgsqlDbType.Integer, sizeof(int), "book_id"));

            const string deleteCommand = "DELETE FROM books WHERE book_id=@book_id";
            adapter.DeleteCommand = new NpgsqlCommand(deleteCommand, c.Connection, t.Transaction);
            adapter.DeleteCommand.Parameters.Add(new NpgsqlParameter("@book_id", NpgsqlDbType.Integer, sizeof(int), "book_id"));

            const string insertCommand = @"
                INSERT INTO books(book_id,name,genre_id,publication_date,publisher_id)
                VALUES(@book_id,@name,@genre_id,@publication_date,@publisher_id)";
            adapter.InsertCommand = new NpgsqlCommand(insertCommand, c.Connection, t.Transaction);
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@book_id", NpgsqlDbType.Integer, sizeof(int), "book_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@genre_id", NpgsqlDbType.Integer, sizeof(int), "genre_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@publication_date", NpgsqlDbType.Date, 128, "publication_date"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@publisher_id", NpgsqlDbType.Integer, sizeof(int), "publisher_id"));

            adapter.Update(s, "books");
        }
    }
}
