﻿using Npgsql;
using NpgsqlTypes;

namespace TRPDISLab.DataBaseAccessors
{
    public class GenresAccessor : AbstractAccessor
    {
        public override void Read(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string selectCommand = "SELECT * FROM genres ORDER BY genre_id";
            adapter.SelectCommand = new NpgsqlCommand(selectCommand, c.Connection, t.Transaction);
            adapter.Fill(s, "genres");
        }

        public override void Update(AbstractConnection c, AbstractTransaction t, LibraryDataSet s)
        {
            var adapter = new NpgsqlDataAdapter();
            const string updateCommand = @"
                UPDATE genres
                SET name=@name
                WHERE genre_id=@genre_id";
            adapter.UpdateCommand = new NpgsqlCommand(updateCommand, c.Connection, t.Transaction);
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));
            adapter.UpdateCommand.Parameters.Add(new NpgsqlParameter("@genre_id", NpgsqlDbType.Integer, sizeof(int), "genre_id"));

            const string deleteCommand = "DELETE FROM genres WHERE genre_id=@genre_id";
            adapter.DeleteCommand = new NpgsqlCommand(deleteCommand, c.Connection, t.Transaction);
            adapter.DeleteCommand.Parameters.Add(new NpgsqlParameter("@genre_id", NpgsqlDbType.Integer, sizeof(int), "genre_id"));

            const string insertCommand = @"
                INSERT INTO genres(genre_id,name)
                VALUES(@genre_id, @name)";
            adapter.InsertCommand = new NpgsqlCommand(insertCommand, c.Connection, t.Transaction);
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@genre_id", NpgsqlDbType.Integer, sizeof(int), "genre_id"));
            adapter.InsertCommand.Parameters.Add(new NpgsqlParameter("@name", NpgsqlDbType.Varchar, 128, "name"));

            adapter.Update(s, "genres");
        }
    }
}
