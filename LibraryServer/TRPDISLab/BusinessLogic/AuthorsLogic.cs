﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class AuthorsLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _authorsAccessor;

        public AuthorsLogic(AbstractAccessor countriesAccessor, AbstractAccessor authorsAccessor)
        {
            _countriesAccessor = countriesAccessor;
            _authorsAccessor = authorsAccessor;
        }

        public override void Read(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, dataSet);
                    _authorsAccessor.Read(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, dataSet);
                    _authorsAccessor.Update(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
