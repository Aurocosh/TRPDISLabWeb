﻿namespace TRPDISLab.BusinessLogic
{
    public interface ILogic
    {
        void Read(LibraryDataSet dataSet);
        void Update(LibraryDataSet dataSet);
    }
}