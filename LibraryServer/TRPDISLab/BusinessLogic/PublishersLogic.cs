﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class PublishersLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _publisherAccessor;

        public PublishersLogic(AbstractAccessor countriesAccessor, AbstractAccessor publisherAccessor)
        {
            _countriesAccessor = countriesAccessor;
            _publisherAccessor = publisherAccessor;
        }

        public override void Read(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, dataSet);
                    _publisherAccessor.Read(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, dataSet);
                    _publisherAccessor.Update(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
