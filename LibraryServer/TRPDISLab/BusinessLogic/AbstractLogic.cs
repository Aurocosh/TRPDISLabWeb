﻿namespace TRPDISLab.BusinessLogic
{
    public abstract class AbstractLogic : ILogic
    {
        public abstract void Read(LibraryDataSet dataSet);
        public abstract void Update(LibraryDataSet dataSet);
    }
}
