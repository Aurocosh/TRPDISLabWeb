﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class AuthorsToBooksLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _publishersAccessor;
        private readonly AbstractAccessor _genresAccessor;
        private readonly AbstractAccessor _booksAccessor;
        private readonly AbstractAccessor _authorsAccessor;
        private readonly AbstractAccessor _authorToBookRelationAccessor;

        public AuthorsToBooksLogic(AbstractAccessor countriesAccessor, AbstractAccessor publishersAccessor, AbstractAccessor genresAccessor, AbstractAccessor booksAccessor, AbstractAccessor authorsAccessor, AbstractAccessor authorToBookRelationAccessor)
        {
            _countriesAccessor = countriesAccessor;
            _publishersAccessor = publishersAccessor;
            _genresAccessor = genresAccessor;
            _booksAccessor = booksAccessor;
            _authorsAccessor = authorsAccessor;
            _authorToBookRelationAccessor = authorToBookRelationAccessor;
        }

        public override void Read(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, dataSet);
                    _publishersAccessor.Read(connection, transaction, dataSet);
                    _genresAccessor.Read(connection, transaction, dataSet);
                    _booksAccessor.Read(connection, transaction, dataSet);
                    _authorsAccessor.Read(connection, transaction, dataSet);
                    _authorToBookRelationAccessor.Read(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, dataSet);
                    _publishersAccessor.Update(connection, transaction, dataSet);
                    _genresAccessor.Update(connection, transaction, dataSet);
                    _booksAccessor.Update(connection, transaction, dataSet);
                    _authorsAccessor.Update(connection, transaction, dataSet);
                    _authorToBookRelationAccessor.Update(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
