﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class BooksLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _publisherAccessor;
        private readonly AbstractAccessor _genresAccessor;
        private readonly AbstractAccessor _booksAccessor;

        public BooksLogic(AbstractAccessor countriesAccessor, AbstractAccessor publisherAccessor, AbstractAccessor genresAccessor, AbstractAccessor booksAccessor)
        {
            _countriesAccessor = countriesAccessor;
            _publisherAccessor = publisherAccessor;
            _genresAccessor = genresAccessor;
            _booksAccessor = booksAccessor;
        }

        public override void Read(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, dataSet);
                    _publisherAccessor.Read(connection, transaction, dataSet);
                    _genresAccessor.Read(connection, transaction, dataSet);
                    _booksAccessor.Read(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update(LibraryDataSet dataSet)
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, dataSet);
                    _publisherAccessor.Update(connection, transaction, dataSet);
                    _genresAccessor.Update(connection, transaction, dataSet);
                    _booksAccessor.Update(connection, transaction, dataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
