﻿using System.Data;
using System.Windows;
using System.Windows.Controls;
using LibraryRemoteClient.LibraryServiceReference;

namespace LibraryRemoteClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataView _booksView;
        private readonly NewRowProvider _rowProvider;
        private readonly libraryServiceSoapClient _libraryService;
        private LibraryDataSet _dataSet;

        public MainWindow()
        {
            InitializeComponent();
            _rowProvider = new NewRowProvider();
            _libraryService = new libraryServiceSoapClient();
        }

        private void GenericHandler(object sender, SelectionChangedEventArgs e) => e.Handled = true;

        #region Changes handling

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (TabControl.SelectedIndex)
            {
                case 0:
                    LoadAuthors();
                    break;
                case 1:
                    LoadAuthorsAndBooks();
                    break;
                case 2:
                    LoadBooks();
                    break;
                case 3:
                    LoadPublishers();
                    break;
            }

        }

        private void DataGridAuthorsSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedValue = DataGridAuthorsSelector.SelectedValue as DataRowView;
            if (selectedValue == null) return;

            var currentAuthor = (int)selectedValue["author_id"];
            _booksView.RowFilter = $"author_id = {currentAuthor}";
            DataGridRelatedBooks.DataContext = _booksView;
            e.Handled = true;
        }

        #endregion

        #region Data loading

        private void LoadAuthors()
        {
            _dataSet = _libraryService.ReadAuthors();
            DockPanelMainAuthors.DataContext = _dataSet;
            ComboBoxAuthorsCountry.ItemsSource = _dataSet.Countries.DefaultView;
        }

        private void LoadAuthorsAndBooks()
        {
            _dataSet = _libraryService.ReadAuthorsXBooks();
            DataGridAuthorsSelector.DataContext = _dataSet;
            DataGridRelatedBooks.DataContext = _dataSet;
            _booksView = new DataView(_dataSet.Author_to_book_relation) { RowFilter = "author_id = 1" };
            DataGridRelatedBooks.DataContext = _booksView;
            ComboBoxAuthorsOfBook.ItemsSource = _dataSet.Books;
        }

        private void LoadBooks()
        {
            _dataSet = _libraryService.ReadBooks();
            DockPanelBooks.DataContext = _dataSet;
            ComboBoxBooksGenre.ItemsSource = _dataSet.Genres;
            ComboBoxBooksPublisher.ItemsSource = _dataSet.Publishers;
        }

        private void LoadPublishers()
        {
            _dataSet = _libraryService.ReadPublishers();
            DockPanelPublishers.DataContext = _dataSet;
            ComboBoxPublisherCountry.ItemsSource = _dataSet.Countries;
        }

        #endregion

        #region Row creation

        private void ButtonAuthorsAdd_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedRow = ((DataRowView) DataGridAuthors.SelectedValue)?.Row;
            var index = _dataSet.Authors.Rows.IndexOf(selectedRow);
            _rowProvider.GetNewAuthorRow(_dataSet, index);
        }

        private void ButtonBooksAdd_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedRow = ((DataRowView)DataGridBooks.SelectedValue)?.Row;
            var index = _dataSet.Books.Rows.IndexOf(selectedRow);
            _rowProvider.GetNewBookRow(_dataSet, index);
        }

        private void ButtonPublishersAdd_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedRow = ((DataRowView)DataGridPublishers.SelectedValue)?.Row;
            var index = _dataSet.Publishers.Rows.IndexOf(selectedRow);
            _rowProvider.GetNewPublisherRow(_dataSet, index);
        }

        private void ButtonAuthorsAndBooksAdd_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedItem = (DataRowView)DataGridAuthorsSelector.SelectedItem;
            var currentAuthorId = (int)(selectedItem?["author_id"] ?? 1);
            _rowProvider.GetNewAtbrRow(_dataSet, currentAuthorId, -1);
        }

        #endregion

        #region Row deletion

        private void ButtonAuthorsRemove_OnClick(object sender, RoutedEventArgs e)
        {
            ((DataRowView)DataGridAuthors.SelectedItem)?.Row.Delete();
        }

        private void ButtonBooksRemove_OnClick(object sender, RoutedEventArgs e)
        {
            ((DataRowView)DataGridBooks.SelectedItem)?.Delete();
        }

        private void ButtonPublishersRemove_OnClick(object sender, RoutedEventArgs e)
        {
            ((DataRowView)DataGridPublishers.SelectedItem)?.Delete();
        }

        private void ButtonAuthorsAndBooksRemove_OnClick(object sender, RoutedEventArgs e)
        {
            ((DataRowView)DataGridRelatedBooks.SelectedItem)?.Delete();
        }

        #endregion

        #region Data saving

        private void ButtonAuthorsSave_OnClick(object sender, RoutedEventArgs e)
        {
            var changes = (LibraryDataSet)_dataSet.GetChanges();
            if (changes == null) return;
            _libraryService.UpdateAuthors(changes);
        }

        private void ButtonAuthorsAndBooksSave_OnClick(object sender, RoutedEventArgs e)
        {
            var changes = (LibraryDataSet)_dataSet.GetChanges();
            if (changes == null) return;
            _libraryService.UpdateAuthorsXBooks(changes);
        }

        private void ButtonBooksSave_OnClick(object sender, RoutedEventArgs e)
        {
            var changes = (LibraryDataSet)_dataSet.GetChanges();
            if (changes == null) return;
            _libraryService.UpdateBooks(changes);
        }

        private void ButtonPublishersSave_OnClick(object sender, RoutedEventArgs e)
        {
            var changes = (LibraryDataSet)_dataSet.GetChanges();
            if (changes == null) return;
            _libraryService.UpdatePublishers(changes);
        }

        #endregion
    }
}
