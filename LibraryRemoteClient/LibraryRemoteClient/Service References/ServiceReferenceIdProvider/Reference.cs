﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LibraryRemoteClient.ServiceReferenceIdProvider {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.CollectionDataContractAttribute(Name="ArrayOfInt", Namespace="http://TRPDISLab/WebServices", ItemName="int")]
    [System.SerializableAttribute()]
    public class ArrayOfInt : System.Collections.Generic.List<int> {
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://TRPDISLab/WebServices", ConfigurationName="ServiceReferenceIdProvider.libraryIdProviderServiceSoap")]
    public interface libraryIdProviderServiceSoap {
        
        // CODEGEN: Generating message contract since element name GetAuthorsIdsResult from namespace http://TRPDISLab/WebServices is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetAuthorsIds", ReplyAction="*")]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponse GetAuthorsIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetAuthorsIds", ReplyAction="*")]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponse> GetAuthorsIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest request);
        
        // CODEGEN: Generating message contract since element name GetAtbrsIdsResult from namespace http://TRPDISLab/WebServices is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetAtbrsIds", ReplyAction="*")]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponse GetAtbrsIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetAtbrsIds", ReplyAction="*")]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponse> GetAtbrsIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest request);
        
        // CODEGEN: Generating message contract since element name GetBooksIdsResult from namespace http://TRPDISLab/WebServices is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetBooksIds", ReplyAction="*")]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponse GetBooksIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetBooksIds", ReplyAction="*")]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponse> GetBooksIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest request);
        
        // CODEGEN: Generating message contract since element name GetPublishersIdsResult from namespace http://TRPDISLab/WebServices is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetPublishersIds", ReplyAction="*")]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponse GetPublishersIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://TRPDISLab/WebServices/GetPublishersIds", ReplyAction="*")]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponse> GetPublishersIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetAuthorsIdsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetAuthorsIds", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequestBody Body;
        
        public GetAuthorsIdsRequest() {
        }
        
        public GetAuthorsIdsRequest(LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class GetAuthorsIdsRequestBody {
        
        public GetAuthorsIdsRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetAuthorsIdsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetAuthorsIdsResponse", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponseBody Body;
        
        public GetAuthorsIdsResponse() {
        }
        
        public GetAuthorsIdsResponse(LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://TRPDISLab/WebServices")]
    public partial class GetAuthorsIdsResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetAuthorsIdsResult;
        
        public GetAuthorsIdsResponseBody() {
        }
        
        public GetAuthorsIdsResponseBody(LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetAuthorsIdsResult) {
            this.GetAuthorsIdsResult = GetAuthorsIdsResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetAtbrsIdsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetAtbrsIds", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequestBody Body;
        
        public GetAtbrsIdsRequest() {
        }
        
        public GetAtbrsIdsRequest(LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class GetAtbrsIdsRequestBody {
        
        public GetAtbrsIdsRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetAtbrsIdsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetAtbrsIdsResponse", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponseBody Body;
        
        public GetAtbrsIdsResponse() {
        }
        
        public GetAtbrsIdsResponse(LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://TRPDISLab/WebServices")]
    public partial class GetAtbrsIdsResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetAtbrsIdsResult;
        
        public GetAtbrsIdsResponseBody() {
        }
        
        public GetAtbrsIdsResponseBody(LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetAtbrsIdsResult) {
            this.GetAtbrsIdsResult = GetAtbrsIdsResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetBooksIdsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetBooksIds", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequestBody Body;
        
        public GetBooksIdsRequest() {
        }
        
        public GetBooksIdsRequest(LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class GetBooksIdsRequestBody {
        
        public GetBooksIdsRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetBooksIdsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetBooksIdsResponse", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponseBody Body;
        
        public GetBooksIdsResponse() {
        }
        
        public GetBooksIdsResponse(LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://TRPDISLab/WebServices")]
    public partial class GetBooksIdsResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetBooksIdsResult;
        
        public GetBooksIdsResponseBody() {
        }
        
        public GetBooksIdsResponseBody(LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetBooksIdsResult) {
            this.GetBooksIdsResult = GetBooksIdsResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPublishersIdsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetPublishersIds", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequestBody Body;
        
        public GetPublishersIdsRequest() {
        }
        
        public GetPublishersIdsRequest(LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class GetPublishersIdsRequestBody {
        
        public GetPublishersIdsRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetPublishersIdsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetPublishersIdsResponse", Namespace="http://TRPDISLab/WebServices", Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponseBody Body;
        
        public GetPublishersIdsResponse() {
        }
        
        public GetPublishersIdsResponse(LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://TRPDISLab/WebServices")]
    public partial class GetPublishersIdsResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetPublishersIdsResult;
        
        public GetPublishersIdsResponseBody() {
        }
        
        public GetPublishersIdsResponseBody(LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetPublishersIdsResult) {
            this.GetPublishersIdsResult = GetPublishersIdsResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface libraryIdProviderServiceSoapChannel : LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class libraryIdProviderServiceSoapClient : System.ServiceModel.ClientBase<LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap>, LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap {
        
        public libraryIdProviderServiceSoapClient() {
        }
        
        public libraryIdProviderServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public libraryIdProviderServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public libraryIdProviderServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public libraryIdProviderServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponse LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetAuthorsIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest request) {
            return base.Channel.GetAuthorsIds(request);
        }
        
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetAuthorsIds() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequestBody();
            LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponse retVal = ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetAuthorsIds(inValue);
            return retVal.Body.GetAuthorsIdsResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponse> LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetAuthorsIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest request) {
            return base.Channel.GetAuthorsIdsAsync(request);
        }
        
        public System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsResponse> GetAuthorsIdsAsync() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAuthorsIdsRequestBody();
            return ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetAuthorsIdsAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponse LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetAtbrsIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest request) {
            return base.Channel.GetAtbrsIds(request);
        }
        
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetAtbrsIds() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequestBody();
            LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponse retVal = ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetAtbrsIds(inValue);
            return retVal.Body.GetAtbrsIdsResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponse> LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetAtbrsIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest request) {
            return base.Channel.GetAtbrsIdsAsync(request);
        }
        
        public System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsResponse> GetAtbrsIdsAsync() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetAtbrsIdsRequestBody();
            return ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetAtbrsIdsAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponse LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetBooksIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest request) {
            return base.Channel.GetBooksIds(request);
        }
        
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetBooksIds() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequestBody();
            LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponse retVal = ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetBooksIds(inValue);
            return retVal.Body.GetBooksIdsResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponse> LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetBooksIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest request) {
            return base.Channel.GetBooksIdsAsync(request);
        }
        
        public System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsResponse> GetBooksIdsAsync() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetBooksIdsRequestBody();
            return ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetBooksIdsAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponse LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetPublishersIds(LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest request) {
            return base.Channel.GetPublishersIds(request);
        }
        
        public LibraryRemoteClient.ServiceReferenceIdProvider.ArrayOfInt GetPublishersIds() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequestBody();
            LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponse retVal = ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetPublishersIds(inValue);
            return retVal.Body.GetPublishersIdsResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponse> LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap.GetPublishersIdsAsync(LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest request) {
            return base.Channel.GetPublishersIdsAsync(request);
        }
        
        public System.Threading.Tasks.Task<LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsResponse> GetPublishersIdsAsync() {
            LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest inValue = new LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequest();
            inValue.Body = new LibraryRemoteClient.ServiceReferenceIdProvider.GetPublishersIdsRequestBody();
            return ((LibraryRemoteClient.ServiceReferenceIdProvider.libraryIdProviderServiceSoap)(this)).GetPublishersIdsAsync(inValue);
        }
    }
}
