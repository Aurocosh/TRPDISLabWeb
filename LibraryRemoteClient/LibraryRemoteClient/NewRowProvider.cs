﻿using System;
using System.Collections.Generic;
using System.Data;
using LibraryRemoteClient.LibraryServiceReference;
using LibraryRemoteClient.ServiceReferenceIdProvider;

namespace LibraryRemoteClient
{
    public class NewRowProvider
    {
        private readonly Queue<int> _authorsIds;
        private readonly Queue<int> _booksIds;
        private readonly Queue<int> _atbrIds;
        private readonly Queue<int> _publisherIds;

        private readonly libraryIdProviderServiceSoapClient _idProviderService;

        public NewRowProvider()
        {
            _authorsIds = new Queue<int>();
            _booksIds = new Queue<int>();
            _atbrIds = new Queue<int>();
            _publisherIds = new Queue<int>();
            _idProviderService = new libraryIdProviderServiceSoapClient();
        }

        public void GetNewAuthorRow(LibraryDataSet dataSet, int index)
        {
            lock (_authorsIds)
            {
                if (_authorsIds.Count == 0)
                {
                    var newIds = _idProviderService.GetAuthorsIds();
                    newIds.ForEach(v => _authorsIds.Enqueue(v));
                }
                var row = dataSet.Authors.NewRow();
                row["author_id"] = _authorsIds.Dequeue();
                row["name"] = "Noname";
                row["surname"] = "Nosurname";
                row["country_id"] = 1;
                if (index != -1)
                    dataSet.Authors.Rows.InsertAt(row, index);
                else
                    dataSet.Authors.Rows.Add(row);
            }
        }

        public void GetNewAtbrRow(LibraryDataSet dataSet, int currentAuthor, int index)
        {
            lock (_atbrIds)
            {
                if (_atbrIds.Count == 0)
                {
                    var newIds = _idProviderService.GetAtbrsIds();
                    newIds.ForEach(v => _atbrIds.Enqueue(v));
                }
                var row = dataSet.Author_to_book_relation.NewRow();
                row["atbr_id"] = _atbrIds.Dequeue();
                row["author_id"] = currentAuthor;
                row["book_id"] = 1;
                if (index != -1)
                    dataSet.Author_to_book_relation.Rows.InsertAt(row, index);
                else
                    dataSet.Author_to_book_relation.Rows.Add(row);
            }
        }

        public void GetNewBookRow(LibraryDataSet dataSet, int index)
        {
            lock (_booksIds)
            {
                if (_booksIds.Count == 0)
                {
                    var newIds = _idProviderService.GetBooksIds();
                    newIds.ForEach(v => _booksIds.Enqueue(v));
                }
                var row = dataSet.Books.NewRow();
                row["book_id"] = _booksIds.Dequeue();
                row["name"] = "Noname";
                row["genre_id"] = 1;
                row["publisher_id"] = 1;
                row["publication_date"] = DateTime.Now;
                if (index != -1)
                    dataSet.Books.Rows.InsertAt(row, index);
                else
                    dataSet.Books.Rows.Add(row);
            }
        }

        public void GetNewPublisherRow(LibraryDataSet dataSet, int index)
        {
            lock (_publisherIds)
            {
                if (_publisherIds.Count == 0)
                {
                    var newIds = _idProviderService.GetPublishersIds();
                    newIds.ForEach(v => _publisherIds.Enqueue(v));
                }
                var row = dataSet.Publishers.NewRow();
                row["publisher_id"] = _publisherIds.Dequeue();
                row["name"] = "Noname";
                row["country_id"] = 1;
                if (index != -1)
                    dataSet.Publishers.Rows.InsertAt(row, index);
                else
                    dataSet.Publishers.Rows.Add(row);
            }
        }
    }
}
