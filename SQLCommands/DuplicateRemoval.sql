DELETE FROM countries
WHERE country_id IN (
	SELECT country_id
	FROM (SELECT country_id, ROW_NUMBER() OVER (PARTITION BY name ORDER BY country_id) AS rnum
		 FROM countries) t
	WHERE t.rnum > 1);
	
DELETE FROM genres
WHERE genre_id IN (
	SELECT genre_id
	FROM (SELECT genre_id, ROW_NUMBER() OVER (PARTITION BY name ORDER BY genre_id) AS rnum
		 FROM genres) t
	WHERE t.rnum > 1);